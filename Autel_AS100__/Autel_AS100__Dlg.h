﻿
// Autel_AS100__Dlg.h: 头文件
//

#pragma once
#include <queue>
#include "ControlCAN.h"
#include "common.h"
#include "kvaDbLib.h"
#include "can_dbc_protocol_lib.h"
//#include <string>
using namespace std;

#define READ_CSV_LEN 32

typedef struct _can_dbc_enm_val
{
	char describe[DATA_LEN_32];							// 目前长度设置16Byte
	int val;
}CAN_DBC_ENM_VAL_ST;

typedef struct _can_dbc_info
{
	char name[DATA_LEN_32];								// 信号名字
	//char qname[DATA_LEN_32];							// 类关系属性名字 test01.a_info.acc_x
	//char commit[DATA_LEN_32];							// 信号描述
	char unit[DATA_LEN_16];								// 单位

	CAN_DBC_ST dbc_cfg;									// dbc配置信息
	// 枚举的值
	CAN_DBC_ENM_VAL_ST enum_value[DBC_ENUM_VAL_NUM];	// .dbc文件中，以 VAL_ 开头的数据
	volatile int enm_cnt;
}CAN_DBC_INFO_ST;

typedef struct _can_dbc_recv_info
{
	char name[DATA_LEN_32];								// 消息名字--仅打印显示
	UINT32 frame_id;									// 帧ID

	CAN_DBC_INFO_ST sig[DBC_SIG_NUM];					// signal content
	volatile int sig_cnt;								// signal number
}CAN_DBC_RECV_INFO_ST;

// CAutelAS100Dlg 对话框
class CAutelAS100Dlg : public CDialogEx
{
// 构造
public:
	CAutelAS100Dlg(CWnd* pParent = nullptr);			// 标准构造函数
	~CAutelAS100Dlg();	//析构函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AUTEL_AS100___DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	queue<CAN_Show_Info> m_ssrInfo_queue;		// 存储CAN数据队列-道通角雷达ASR100
	queue<CAN_Show_Info> m_canSpeed_queue;		// 存储CAN数据队列:0x760车速信息
	struct SRRObstacle m_srr_obstacle_info[3];
	struct VehicleCan vehicleCan_info;

	// btn采集按钮标志
	bool m_btn_func_sw;
	// 接受线程
	bool m_thread_recv_is_stop;

	// 处理线程
	bool m_thread_process_is_stop;		// true停止，false执行中
	bool m_use_dbc_info_parse_thread_is_stop;// true停止，false执行中
	bool m_thread_process_is_working;	// true工作中，false等待中

	CEdit m_edit_object_num_handle;
	CEdit m_edit_move_object_num_handle;
	CEdit m_edit_stop_object_num_handle;
	CEdit m_edit_static_object_num_handle;
	CListBox m_lis_show_info_handle;
	CEdit m_edit_vehicle_spd_handle;
	CEdit m_imu_x_acc_handle;
	CEdit m_imu_z_yaw_handle;
	CComboBox m_combox_can_index_handle;
	CButton m_btn_func_sw_handle;
	CMFCEditBrowseCtrl m_editBrowse_handle;
	CButton m_btn_cvs_analysis_handle;

	// listbox显示内容
	CComboBox m_combox_show_listbox_handle;
	bool m_listbox_is_show;

	// 用时间对数据进行过滤
	CComboBox m_combox_begin_hour_handle;
	CComboBox m_combox_begin_min_handle;
	CComboBox m_combox_end_hour_handle;
	CComboBox m_combox_end_min_handle;
	char m_time_begin_filter_str[READ_CSV_LEN];
	char m_time_end_filter_str[20];
	bool m_enable_time_filter;			// true使能时间过滤，false不使能时间过滤
	bool m_seeked_start_time;			// false 未找到，true找到
	

	void In_Listbox_Show_SRR_Info(struct SRRObstacle *p_info);
	void List_Show_Info(char* data);
	// 写 txt文件操作
	//FILE* txtFile_handle;							// 写文件句柄
	//int write_times = 0;
	//void Create_txt_File_To_Save_Log_Info();

	// 获取CSV文件信息
	bool m_parse_process_stt;
	CFile m_csv_rfile_handle;						// 读句柄
	bool m_thread_csv_is_stop;
	bool m_csv_is_reading;							// true正读取中，false已关闭
	static UINT unpack_csv_src_file_thread(void *param);

	// 写csv文件操作
	CFile m_csv_wfile_handle;						// 写句柄
	CString m_w_csv_file_name;						// 写csv文件名字

	bool create_csv_file_to_save_unpack_res_opt();
	bool write_data_to_csv_file_opt(CStringA w_cstr, uint8_t msg_index, char* time_str, uint32_t frame_id);

	// parse dbc file  已经局部变量申请空间由1MB扩展到4MB
	CAN_DBC_RECV_INFO_ST m_can_dbc_handle[DBC_MSG_NUM];// 最多能解析64组消息（64个帧ID对应包数据）
	volatile int frm_cnt;
	bool dbc_parse_ok;								// true成功，false失败
	queue<CAN_Show_Info> m_dbc_recv_queue;			// dbc接收文件队列


	bool load_can_dbc_cfg_file_opt(char* dbc_name);
	KvaDbStatus unpack_can_dbc_file_opt(char* filename);
	void Debug_Print_DBC_Info();
	static UINT Can_Recv_Thread(void *param);
	static UINT Can_Process_Thread(void *param);
	//static UINT Can_Use_DBC_Process_Thread(void* param);
	static UINT use_can_dbc_cfg_to_unpack_csv_file_opt_thread(void* param);

	bool judge_csv_file_time_condition();
	bool task_end_opt();
	bool load_can_dbc_cfg_file_opt();
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void Dlg_Front_Init();
	bool mySetFontSize(int ID, float font_size);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedButton2();
	CComboBox m_combox_begin_second_handle;
	CComboBox m_combox_end_second_handle;
	CMFCEditBrowseCtrl m_edtBrowse_dbc_handle;
	CComboBox m_cmbox_csv_offset_handle;
};
