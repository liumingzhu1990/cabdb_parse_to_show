#pragma once
#ifndef CAN_DBC_PROTOCOL_LIB_H_
#define CAN_DBC_PROTOCOL_LIB_H_
#include <stdint.h>
#include <stdbool.h>
// 大小端模式
enum
{
    INTEL_MODE,         // 小端模式
    MOTOROLA_MODE,      // 大端模式
};
// 有无符号
enum
{
    SG_UNSIGNED,        // 无符号信号
    SG_SIGNED = 1,      // 有符号信号
};

// 确定数据类型（主要决定调用库的执行逻辑）
typedef enum _dbc_data_type_em
{
    DATA_TYPE_VALID,    // 无效类型
    DATA_TYPE_IS_INT,	// 数据类型不是float类型（无符号类型）
    DATA_TYPE_IS_FLT,	// 数据类型是float类型（有符号类型）
//	DATA_TYPE_IS_DBL,	// 数据类型是double类型(暂时不支持，因嵌入式系统暂不支持double类型)
}DBC_DATA_TYPE_EM;
// CAN dbc结构体
typedef struct _can_dbc_st
{
    //uint32_t 		frame_id;				// 帧ID

    uint8_t			byte_order;				// 字节序
    uint8_t 		start_bit;				// 开始bit
    uint8_t 		bit_count;				// 有效bit数
    float           init_val;               // 初始值
    float 			factor;					// 比例因数
    float 			offset;					// 补偿值
    float 			min;					// 最小值
    float 			max;					// 最大值
    uint8_t 		sign_bit;				// 符号位，【0：unsigned】【1:signed】
    uint8_t         data_type;              // 数据类型【0：int】【1：float】【2:double暂不支持】
}CAN_DBC_ST;		// 9+20=29

/************************************************************************
* Description : CAN打包解包的字节序和数据类型整合
* Parameters  : (都是DBC相关的信息)
                byte_order: 字节序      [0:大端motorola];   [1:小端intel];
                is_signed:  有无符号    [0:unsigned];       [1:signed / float / double];
                value_type: 数据类型    [0:Integer(unsigned/signed)]; 暂时用不到（[1:float]; [2:double]）;
* Returns     : uint32_t, 集合
* Notes       :
************************************************************************/
#define get_can_sig_msg_macro_def(byte_order, is_signed, value_type)  ((((byte_order)&0xFF) << 16) | (((is_signed)&0xFF) << 8) | ((value_type)&0xFF))

float use_dbc_cfg_params_unpack_can_msg_api(CAN_DBC_ST* p_can_dbc_st, uint8_t* indata, uint32_t invalid_val);   // 解包
bool use_dbc_cfg_params_pack_can_msg_api(float value, CAN_DBC_ST* p_can_st, uint8_t* outdata);                  // 封包

#endif /* CAN_DBC_PROTOCOL_LIB_H_ */