#include "can_dbc_protocol_lib.h"
#include <stdio.h>


// 数据类型
enum
{
    DATA_TYPE_INT,      // int(分有符号和无符号)，目前使用于全部
    DATA_TYPE_FLT,      // float
    //DATA_TYPE_DBL,      // double(暂时用不到)
};

//获得U32的n位的值
#define get_bit_n_value(dword, n, offset)         (((uint32_t)(dword) >> (offset)) & ((1UL << (n)) - 1))
void set_bit_of_u8_value(uint8_t* pbyData, uint8_t n, uint8_t byOffset, uint8_t by_val);

/************************************************************************
* Description : 设置8位值中的N位的值
* Parameters  : pbyData，需要改的值指针
                n，修改的位数
                byOffset，偏移
                by_val，设置值
* Returns     : none
* Notes       :
************************************************************************/
void set_bit_of_u8_value(uint8_t* pbyData, uint8_t n, uint8_t byOffset, uint8_t by_val)
{
    uint8_t tmp = *pbyData;
    uint8_t mask;
    mask = (((uint8_t)1u << n) - 1) << byOffset;
    tmp &= ~mask;                                    //先清0
    tmp |= (by_val << byOffset) & mask;             //值也需要保证没有更多的位
    *pbyData = tmp;
}
#if 0
/************************************************************************
* Description : 设置32位值中的N位的值
* Parameters  : pdwData，需要改的值指针
                n，修改的位数
                byOffset，偏移
                dwValue，设置值
* Returns     : none
* Notes       :
************************************************************************/
//void set_bit_of_u32_value(uint32_t* pdwData, uint8_t n, uint8_t byOffset, uint32_t dwValue)
//{
//    uint32_t tmp = *pdwData;
//    uint32_t mask;
//    mask = (((uint32_t)1u << n) - 1) << byOffset;
//    tmp &= ~mask;                                    //先清0
//    tmp |= (dwValue << byOffset) & mask;             //值也需要保证没有更多的位
//    *pdwData = tmp;
//}

/************************************************************************
* Description : 设置64位值中的N位的值
* Parameters  : pldwData，需要设置的值的指针
                n，修改的位数
                byOffset，偏移值
                ldwValue，设置值
* Returns     : none
* Notes       : n和byOffset均不能超过63
************************************************************************/
//void set_nbit_of_u64_value(uint64_t* pldwData, uint8_t n, uint8_t byOffset, uint64_t ldwValue)
//{
//    uint64_t tmp;
//    uint64_t mask;
//
//    if (pldwData == NULL)
//    {
//        return;
//    }
//    mask = (((uint64_t)1 << n) - 1) << byOffset;
//    tmp = *pldwData;
//    tmp &= ~mask;                                        //先清0
//    tmp |= (ldwValue << byOffset) & mask;                //值也需要保证没有更多的位;
//    *pldwData = tmp;
//}
#endif

union FloatInt
{
    int32_t   	i;
    float		f;
};
// data type:float to int
int32_t float_to_int(float f)
{
    union FloatInt ret, bias;
    ret.f = f;
    bias.i = (23 + 127) << 23;
    if (f < 0.0f) { bias.i = ((23 + 127) << 23) + (1 << 22); }
    ret.f += bias.f;
    ret.i -= bias.i;
    return ret.i;
}
//// data type:int to float
//float int_to_float(int32_t i)
//{
//	union FloatInt ret, bias;
//    ret.i = i;
//    bias.i = (23 + 127) << 23;
//    if(i < 0){bias.i = ((23 + 127) << 23) + (1 << 22);}
//    ret.i += bias.i;
//    ret.f -= bias.f;
//    return ret.f;
//}
/************************************************************************
* Description : CAN报文打包 发送的前置
* Parameters  : val_msg，字节序和数据类型等信息的一个集合 见get_can_sig_msg_macro_def()
                value, 值
                outdata, 输出值
                can dbc结构体

* Returns     :
* Notes       : 1）只支持8字节数据 有符号/无符号（整型）、浮点型；大小端
                另外 嵌入式中不支持double，将其处理成float，若移植为
                其他计算机用途，注意修改
                2）因运行效率问题，取消使用int64_t(l_i32_bin_val、l_i32_mask、l_u32_signed_mask)
                改为int32_t，若有需要使用double，则需要改回
                //另外 负浮点型直接强制转化为unsigned，是未定义的行为
************************************************************************/
static bool pack_can_dbc_data_api(uint32_t val_msg, float value, uint8_t* outdata, CAN_DBC_ST* can_st)
{
    bool res = false;
    float l_f_tmp = 0.0f;           // 处理临时值
    int32_t l_i32_bin_val = 0;      // 处理二进制值

    int8_t byte_index = 0;          // 当前操作的data[byte_index]
    int8_t bit_offset = 0;          // 在data[byte_index]中的起始bit
    int8_t val_pos = 0;             // 当前处理的在l_u32_bin_val的bit位置
    int8_t deal_len = 0;            // 处理的数据位数
    int8_t remain_len = 0;          // 剩余未处理的数据位数
    uint8_t by_val = 0;             // 设置值

    // 异常处理
    if (outdata == NULL) return res;

    // 判最值，超出后按照最值进行处理
    if (value > can_st->max)
    {
        printf("(%0.4f)大于最大值,按照最大值(%0.4f)处理.\r\n", value, can_st->max);
        value = can_st->max;
    }
    else if (value < can_st->min)
    {
        printf("(%0.4f)小于最小值,按照最小值(%0.4f)处理.\r\n", value, can_st->min);
        value = can_st->min;
    }

    // 计算value值,初始值init_val在没有给数，需要发送时使用，目前用不到
    l_f_tmp = (value - can_st->offset) / can_st->factor;
    //    l_f_tmp = (value - can_st->init_val - can_st->offset) / can_st->factor;

    //    printf( "value:%0.4f, start_bit:%d, bit_len:%d, init_val:%0.4f, offset:%0.4f, factor:%0.6f, max:%0.4f,"
    //    		" min:%0.4f, l_f_tmp:%0.4f\r\n", value, can_st->start_bit, can_st->bit_count, can_st->init_val, can_st->offset,
    //    		can_st->factor, can_st->max, can_st->min, l_f_tmp);
        // 【判数据类型】获得数据的二进制码
    switch (val_msg & 0xFF)
    {
    case DATA_TYPE_INT: 								// interge整型
    {
        if (((val_msg >> 8) & 0xFF) == SG_UNSIGNED)    	//无符号型
        {
            if (l_f_tmp < 0)
            {
                printf("unsigned data(%0.1f) < 0, treat it as 0.\r\n", l_f_tmp);
                l_f_tmp = 0;
            }
        }
        l_i32_bin_val = (int32_t)l_f_tmp;
        //        printf("[int]l_i32_bin_val:%d, l_f_tmp:%0.4f\r\n", l_i32_bin_val, l_f_tmp);
    }break;
    case DATA_TYPE_FLT: 								// float浮点类型
    {
        if (can_st->bit_count > 32) l_i32_bin_val = 0;
        else l_i32_bin_val = float_to_int(l_f_tmp);

        //        printf( "[flt]l_i32_bin_val:%d\r\n", l_i32_bin_val);
    }break;
    //    case DATA_TYPE_DBL:  								// double   嵌入式式系统暂不支持double
    default:
    {
#if 0       //double的预留代码
        if (can_st->bit_count == 64)  l_i32_bin_val = 0;
        else
        {
            double dtmp = l_f_tmp;
            l_i32_bin_val = *(int64_t*)&dtmp;
        }
#endif
        l_i32_bin_val = 0;
    }break;
    }
    //    printf( "l_i32_bin_val:%d, 0x%08X\r\n", l_i32_bin_val, l_i32_bin_val);
        // buff填充 从signal的低字节处理，逐字节处理
    byte_index = can_st->start_bit >> 3;
    remain_len = can_st->bit_count;
    bit_offset = can_st->start_bit - (byte_index << 3);
    val_pos = 0;

    // 【大小端模式】移位操作
    if (((val_msg >> 16) & 0xFF) == INTEL_MODE)    		// Intel 小端模式
    {
        do
        {
            deal_len = (remain_len < 8 - bit_offset) ? remain_len : (8 - bit_offset);
            by_val = (uint8_t)(l_i32_bin_val >> val_pos);

            set_bit_of_u8_value(outdata + byte_index, deal_len, bit_offset, by_val);

            byte_index++;
            val_pos += deal_len;
            remain_len -= deal_len;
            bit_offset = 0;

        } while (remain_len);
    }
    else                            					// motorola 大端模式
    {
        do
        {
            deal_len = (remain_len < 8 - bit_offset) ? remain_len : (8 - bit_offset);
            by_val = (uint8_t)(l_i32_bin_val >> val_pos);

            set_bit_of_u8_value(outdata + byte_index, deal_len, bit_offset, by_val);

            byte_index--;
            val_pos += deal_len;
            remain_len -= deal_len;
            bit_offset = 0;

        } while (remain_len);
    }
    //	debug_hex_print(SERIAL_UART4, "CAN clc", outdata, 8);	// 临时测试

    res = true;
    return res;
}

///************************************************************************
//* Description : CAN报文解包
//* Parameters  : val_msg，字节序和数据类型等信息的一个集合 见get_can_sig_msg_macro_def()
//				intdata, 输入数据
//                can dbc结构体
//
//* Returns     : 解析结果
//* Notes       : 1）只支持8字节数据 有符号/无符号（整型）、浮点型；大小端
//                另外 嵌入式中不支持double，将其处理成float，若移植为
//                其他计算机用途，注意修改
//                此函数直接根据dbc做处理
//                2）因运行效率问题，取消使用int64_t(l_i32_bin_val、l_i32_mask、l_u32_signed_mask)
//                改为int32_t，若有需要使用double，则需要改回
//************************************************************************/
float unpack_can_dbc_data_api(uint32_t val_msg, uint8_t* indata, CAN_DBC_ST* can_st, uint32_t invalid_val)
{
    float ret_val = 0;              // 返回值
    int32_t l_i32_tmp = 0;          // 处理临时值
    int32_t l_i32_bin_val = 0;      // 处理临时bin值
    int32_t l_i32_mask = 0;         // 掩码
    uint32_t l_u32_signed_mask = 0; // 有符号掩码

    int8_t byte_index = 0;          // 当前操作的data[byte_index]
    int8_t val_pos = 0;             // 当前处理的在l_i32_bin_val的bit位置
    int8_t bit_offset = 0;          // 在data[byte_index]中的起始bit
    int8_t deal_len = 0;            // 处理的数据位数
    int8_t remain_len = 0;          // 剩余未处理的数据位数

    // 异常处理
    if (indata == NULL) return 0.0f;

    //    printf("start_bit:%d, bit_len:%d, init_val:%0.4f, offset:%0.4f, factor:%0.6f\r\n",
    //    		can_st->start_bit, can_st->bit_count, can_st->init_val, can_st->offset, can_st->factor);

        // 从signal的低字节处理，逐字节处理
    byte_index = can_st->start_bit >> 3;
    remain_len = can_st->bit_count;
    bit_offset = can_st->start_bit - (byte_index << 3);
    val_pos = 0;

    // 【大小端模式】移位
    if (((val_msg >> 16) & 0xFF) == INTEL_MODE)    // Intel小端模式
    {
        do
        {
            deal_len = (remain_len < 8 - bit_offset) ? remain_len : (8 - bit_offset);

            l_i32_tmp = get_bit_n_value(indata[byte_index], deal_len, bit_offset);
            l_i32_bin_val += (l_i32_tmp << val_pos);

            byte_index++;
            remain_len -= deal_len;
            bit_offset = 0;
            val_pos += deal_len;

        } while (remain_len);
    }
    else                            				// motorola大端模式
    {
        do
        {
            deal_len = (remain_len < 8 - bit_offset) ? remain_len : (8 - bit_offset);

            l_i32_tmp = get_bit_n_value(indata[byte_index], deal_len, bit_offset);
            l_i32_bin_val += (l_i32_tmp << val_pos);

            byte_index--;
            remain_len -= deal_len;
            bit_offset = 0;
            val_pos += deal_len;

        } while (remain_len);
    }
    //    printf("l_i32_bin_val:%d\r\n", l_i32_bin_val);
        // 【判有无符号】
    if (((val_msg >> 8) & 0xFF) == SG_SIGNED)		// 有符号
    {
        l_i32_mask = (1UL << (can_st->bit_count - 1));
        if ((l_i32_bin_val & l_i32_mask) == l_i32_mask)
        {
            l_u32_signed_mask = ~((1UL << can_st->bit_count) - 1);
            l_i32_bin_val |= (-1L & l_u32_signed_mask);     //高位填充，扩展为32位/64位
        }
    }
    //    printf("l_i32_bin_val:%d\r\n", l_i32_bin_val);

        // 判断是否是有效的数据，不是数据就为0
    if (((invalid_val >> 31) & 0x01) == 0x01)			// 使能对无效值的过滤功能
    {
        //    	printf( "invail_val:%ld, l_i32_bin_val:%d\r\n", (invalid_val & 0x7FFFFFFF), l_i32_bin_val);
        if ((invalid_val & 0x7FFFFFFF) == l_i32_bin_val)	// 无效值
        {
            goto DBC_UNPACK_RES;
        }
    }

    // 【判数据类型】
    switch (val_msg & 0xFF)
    {
    case DATA_TYPE_INT:     					// interge整型
    {
        ret_val = (float)(l_i32_bin_val * can_st->factor) + can_st->offset;
        //			printf("[int]%d, ret_val:%0.4f\r\n", (int32_t)ret_val, ret_val);
    } break;
    case DATA_TYPE_FLT:     					// float浮点类型
    {
        //			printf("[flt]%ld, factor:%0.4f, offset:%0.2f\r\n", l_i32_bin_val, can_st->factor, can_st->offset);
        if (can_st->bit_count > 32) ret_val = 0;
        else ret_val = (float)(l_i32_bin_val)*can_st->factor + can_st->offset;
        //			printf("[flt]%d, ret_val:%0.4f\r\n", (int32_t)ret_val, ret_val);
    } break;
    case 2:     								// double  嵌入式不支持
    default:
    {
#if 0 //double的预留代码
        if (can_st->bit_count != 64) ret_val = 0;
        else ret_val = (float)(*(double*)(&l_i32_bin_val) * can_st->factor + can_st->offset);
#endif
        ret_val = 0;
    } break;
    }

DBC_UNPACK_RES:
    // 判最值
    if (ret_val > can_st->max) ret_val = can_st->max;
    else if (ret_val < can_st->min) ret_val = can_st->min;
    //    printf("%d, ret_val:%0.4f\r\n", (int32_t)ret_val, ret_val);

    return ret_val;
}


/*
 * 【解包/拆包】、CAN动态解包CAN信号数据
 * 参数1：CAN数据结构体，输入数据（data[8]）,（过滤）无效值
 * 返回：解析的数据；
 * 说明：默认返回类型float，需要解析后进行类型转换操作；【注意】被无效值过滤后返回数据为0.0
 */
float use_dbc_cfg_params_unpack_can_msg_api(CAN_DBC_ST* p_can_dbc_st, uint8_t* indata, uint32_t invalid_val)
{
    float ret_f_data = 0.0f;

#if 0		// debug print
    uint8_t de_print[100] = { 0 };
    sprintf(de_print, "byte:%d,start_bit:%d, len:%d, factor:%0.8f, offset:%0.4f\r\n ", p_can_dbc_st->byte_order, \
        p_can_dbc_st->start_bit, p_can_dbc_st->bit_count, \
        p_can_dbc_st->factor, p_can_dbc_st->offset);
    printf("%s\r\n", de_print);
#endif

    // 解包操作：根据数据类型是否float，来判断是采用有符号解析还是无符号解析
    uint32_t val_msg = 0;
    //	val_msg = get_can_sig_msg_macro_def(p_can_dbc_st->byte_order, SG_UNSIGNED, DATA_TYPE_INT);
    switch (p_can_dbc_st->data_type)
    {
    case DATA_TYPE_IS_INT:	// int 无符号
    {
        val_msg = get_can_sig_msg_macro_def(p_can_dbc_st->byte_order, p_can_dbc_st->sign_bit, DATA_TYPE_INT);
    }break;
    case DATA_TYPE_IS_FLT:	// float 有符号
    {
        val_msg = get_can_sig_msg_macro_def(p_can_dbc_st->byte_order, SG_SIGNED, DATA_TYPE_FLT);
        //			val_msg = get_can_sig_msg_macro_def(p_can_dbc_st->byte_order, p_can_dbc_st->sign_bit, DATA_TYPE_FLT);
    }break;
    default:
    {
        printf("暂时不支持double等其他类型（目前仅支持int(无符号)和float(有符号)类型）。\r\n");
        return ret_f_data;
    }break;
    }
    // 解包操作：解析数据
    ret_f_data = unpack_can_dbc_data_api(val_msg, indata, p_can_dbc_st, invalid_val);
#if 0
    application_print(SERIAL_UART4, "unpack value:%0.6f\r\n", ret_f_data);
#endif
    return ret_f_data;
}
/*
 * 【封包/打包】CAN动态打包CAN信号数据
 * 参数：设值，can dbc结构体，输出数据（data[8]）
 * 返回：false失败，true成功
 */
bool use_dbc_cfg_params_pack_can_msg_api(float value, CAN_DBC_ST* p_can_st, uint8_t* outdata)
{
    bool res = false;
    uint32_t val_msg = 0;

    // 封包操作：根据数据类型是否float，来判断是采用有符号解析还是无符号解析
    switch (p_can_st->data_type)
    {
    case DATA_TYPE_IS_INT:	// int类型，区分有符号和无符号
    {
        //			printf( "data type:int.sign bit:%s\r\n", p_can_st->sign_bit==SG_SIGNED?"signed":"unsigned");
        val_msg = get_can_sig_msg_macro_def(p_can_st->byte_order, p_can_st->sign_bit, DATA_TYPE_INT);
    }break;
    case DATA_TYPE_IS_FLT:	// float类型，不区分有符号和无符号
    {
        //			printf( "data type:float.\r\n");
        val_msg = get_can_sig_msg_macro_def(p_can_st->byte_order, SG_SIGNED, DATA_TYPE_FLT);
        //			val_msg = get_can_sig_msg_macro_def(p_can_st->byte_order, p_can_st->sign_bit, DATA_TYPE_FLT);
    }break;
    default:
    {
        printf("暂时不支持double等其他类型（目前仅支持int(无符号)和float(有符号)类型）。\r\n");
        return res;
    }break;
    }
    // 封包操作：封装数据
    if (pack_can_dbc_data_api(val_msg, value, outdata, p_can_st) == true)
    {
        res = true;
    }

    return res;
}