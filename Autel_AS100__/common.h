#pragma once
#define DATA_LEN_8			8
#define DATA_LEN_10			10
#define DATA_LEN_16			16
#define DATA_LEN_32			32
#define DATA_LEN_64			64
#define DATA_LEN_100		100
#define DATA_LEN_128		128

struct Point2f {
	float x;
	float y;
};

struct SRRObstacle {
	bool is_cipv;
	UINT8 frame_order1;
	UINT8 frame_order2;
	UINT8 frame_order3;
	UINT8 object_id;
	UINT8 tracking_num;
	UINT8 warning_grade;

	bool is_valid_time;
	char time_stamp_str[20];
	ULONGLONG system_time;

	struct Point2f ttc;
	struct Point2f dist;
	struct Point2f vel;
	struct Point2f acc;

	UINT8 warning_zone;
	UINT8 motion_state;
	UINT8 malfunction;

	UINT8 move_obj_num;
	UINT8 stop_obj_num;
	UINT8 static_obj_num;
	UINT8 total_obj_num;

	bool is_valid_carSpeed;
	float car_speed;
	float car_acceleration;
	float car_angle_speed;
};

struct Judge_Str {
	volatile bool is_full_9_frame_sw;		// 队列是否满9组数据
	int pop_cnt;					// pop计数
	int object_num;				// 队列中接收的所有数据

	int dispose_times;			// 要处理的数据条数，9的整数倍
	volatile UINT8 array_index;				// 输出打印的数据下标
	UINT8 array_index_multi;				// 计算下标用到倍数

	volatile UINT8 move_obj_num;
	volatile UINT8 static_obj_num;
	volatile UINT8 stop_obj_num;

};

typedef struct _CAN_Show_Info
{
	VCI_CAN_OBJ CanInfo;
	char	TimeStr[20];	// 解析CSV的时间戳
}CAN_Show_Info;

struct VehicleCan {
	UINT8 right_turn;
	UINT8 left_turn;
	UINT8 reversing;
	UINT8 brake_status;
	float car_speed;
	float yaw_rate;
	float x_accel;
};
enum SRRCarZone {
	UNKNOWN_CAR_ZONE = 0, RIGHT_MIDDLE_CAR_ZONE, RIGHT_BACK_CAR_ZONE, RIGHT_FRONT_CAR_ZONE,
};

enum SRRMOtionState {
	UNKNOWN_MOTION_STATE = 0, MOVING_MOTION_STATE, STOPED_MOTION_STATE, STANDING_MOTION_STATE,
};

enum SRRMalfunction {
	SENSOR_OK = 0, SENSOR_FAILURE, SENSOR_PERFORM_DECREASED, SENSOR_COMMUNICATION_FAILURE,
};

/* 波特率枚举 */
enum _BaudRate
{
	BR_10K = 0,
	BR_20K,
	BR_50K,
	BR_125K,
	BR_250K,
	BR_500K,
	BR_800K,
	BR_1000K
};
/***********************dbc文件解析****************************/
#define  DBC_MSG_NUM			DATA_LEN_64
#define  DBC_SIG_NUM			DATA_LEN_32
#define  DBC_ENUM_VAL_NUM		DATA_LEN_32
/*
说明：能区分标准帧和扩展帧（通过帧ID的最高位区分，flag = frame_id>>31; flag为1扩展帧 ，为0为标准帧），不能区分是远程帧和数据帧；
*/
typedef struct _Can_DBC_Node_Info
{

}Can_DBC_Node_Info;

//typedef struct _Can_DBC_Msg_Info
//{
//	char *name;								// 消息名字
//	//char *qname;							// 类关系属性名字 test01.a_info
//	//char *commit;							// 消息描述
//	//UINT8 dlc;								// CAN数据长度，默认长度为8
//	UINT32 frame_id;								// 帧ID
//	//UINT8 flags;							// 消息标志
//}Can_DBC_Msg_Info;

//typedef struct _can_dbc_enm_val
//{
//	char *describe;							// 目前长度设置16Byte
//	int val;
//}CAN_DBC_ENM_VAL_ST;
//
//typedef struct _can_dbc_info
//{
//	char *name;								// 信号名字
//	//char *qname;							// 类关系属性名字 test01.a_info.acc_x
//	//char *commit;							// 信号描述
//	char *unit;								// 单位
//
//	CAN_DBC_ST dbc_cfg;						// dbc配置信息
//	// 枚举的值
//	CAN_DBC_ENM_VAL_ST enum_value[DBC_ENUM_VAL_NUM];	// .dbc文件中，以 VAL_ 开头的数据
//	volatile int enm_cnt;
//}CAN_DBC_INFO_ST;

//typedef struct _can_dbc_recv_info
//{
//	//Can_DBC_Msg_Info msg;					// frame id 属性
//
//	char* name;								// 消息名字--仅打印显示
//	UINT32 frame_id;								// 帧ID
//
//	CAN_DBC_INFO_ST sig[DBC_SIG_NUM];		// signal number
//	volatile int sig_cnt;
//}CAN_DBC_RECV_INFO_ST;


/*******************CAN 协议解析********************/
typedef enum _Signal_Type_Enum
{
	SIG_NO,
	SIG_INT,
	SIG_FLOAT,
	//SIG_STRING,
}Signal_Type_Enum;
typedef struct _Ret_Signal_Str
{
	Signal_Type_Enum sign_type;
	uint32_t ret_int_value;
	float ret_float_value;
	//char* ret_string_value;
}Ret_Signal_Str;