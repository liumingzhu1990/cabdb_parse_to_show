﻿
// Autel_AS100__Dlg.cpp: 实现文件
//

#include "stdafx.h"
#include "Autel_AS100__.h"
#include "Autel_AS100__Dlg.h"
#include <string.h>
// #include "afxdialogex.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int m_devtype = VCI_USBCAN2;
int m_devind = 0;
int m_CANInd = 0;
bool g_recv_is_stop = false;
/*
 * 打印十六值数据
 */
void debug_hex_print(const char *dbg_info, uint8_t *indata, uint16_t inlen)
{
	printf("\r\n----------hex-------------\r\n");
	printf("%s,len:%d, ", dbg_info, inlen);
	for (uint16_t i = 0; i < inlen; i++)
	{
		printf("%02X ", indata[i]);
	}
	printf("\r\n----------hex-------------\r\n");
}

/*****************************************
函数说明：CAN本身机制，异常消息提示
函数返回值：
*****************************************/
void RecvFrame_ErrorFormation(void *errinfo)
{
	VCI_ERR_INFO *errorInfo = (VCI_ERR_INFO *)(errinfo);
	CString msg;

	switch (errorInfo->ErrCode)
	{
	case 0x0100: // 设备已经打开
	{
		printf("设备已经打开");
	}
	break;
	case 0x0200: // 打开设备错误
	{
		printf("打开设备错误");
	}
	break;
	case 0x0400: // 设备没有打开
	{
		printf("设备没有打开");
	}
	break;
	case 0x0800: // 缓冲区溢出
	{
		printf("缓冲区溢出");
	}
	break;
	case 0x1000: // 此设备不存在
	{
		printf("此设备不存在");
	}
	break;
	case 0x2000: // 装载动态库失败
	{
		printf("装载动态库失败");
	}
	break;
	case 0x4000: // 表示为执行命令失败错误
	{
		printf("表示为执行命令失败错误");
	}
	break;
	case 0x8000: // 内存不足
	{
		printf("内存不足");
	}
	break;
	case 0x0001: // CAN 控制器内部 FIFO 溢出
	{
		printf("CAN 控制器内部 FIFO 溢出");
	}
	break;
	case 0x0002: // CAN 控制器错误报警
	{
		printf("CAN 控制器错误报警");
	}
	break;
	case 0x0004: // CAN 控制器消极错误
	{
		printf("CAN 控制器消极错误");
	}
	break;
	case 0x0008: // CAN 控制器仲裁丢失
	{
		printf("CAN 控制器仲裁丢失");
	}
	break;
	case 0x0010: // CAN 控制器总线错误
	{
		printf("CAN 控制器总线错误");
	}
	break;
	default:
		break;
	}
}
static UINT ReceiveThread1(void *param)
{
	// CAutelAS100Dlg *pthread = (CAutelAS100Dlg*)param;
	VCI_CAN_OBJ frameinfo[50];
	VCI_ERR_INFO errinfo;
	int len = 0;
	int clac_760_times = 0;
	// long can_cache_len = 0;
	printf("recv thread\n");

	while (!g_recv_is_stop)
	{
		// Sleep(1);
		// can_cache_len = VCI_GetReceiveNum(m_devtype, m_devind, m_CANInd);
		// if (can_cache_len <= 0) {
		//	Sleep(1);
		//	continue;
		// }

		len = VCI_Receive(m_devtype, m_devind, m_CANInd, frameinfo, 50, 200);
		if (len <= 0)
		{
			// 注意：如果没有读到数据则必须调用此函数来读取出当前的错误码，
			// 千万不能省略这一步（即使你可能不想知道错误码是什么）
			VCI_ReadErrInfo(m_devtype, m_devind, m_CANInd, &errinfo);
			/* 异常报错弹框 */
			// pthread->RecvFrame_ErrorFormation(&errinfo);
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
				if (frameinfo[i].frame_id >= 0x750 && frameinfo[i].frame_id <= 0x758)
				{
					printf("0x%03X\n", frameinfo[i].frame_id);
				}
			}
		}
	}
	return 1;
}

UINT CAutelAS100Dlg::Can_Recv_Thread(void *param)
{
#define RECV_LEN 50
	CAutelAS100Dlg *pthread = (CAutelAS100Dlg *)param;
	VCI_CAN_OBJ frameinfo[RECV_LEN];
	VCI_ERR_INFO errinfo;
	int len = 0;
	int clac_760_times = 0;
	// long can_cache_len = 0;
	printf("recv thread\n");

	// pthread->m_btn_func_sw_handle.SetWindowTextA("采集中");
	while (!pthread->m_thread_recv_is_stop)
	{
		len = VCI_Receive(m_devtype, m_devind, m_CANInd, frameinfo, RECV_LEN, 10); // 等待函数，10ms-100HZ
		// printf("%d\r\n", len);
		if (len <= 0)
		{
			// 注意：如果没有读到数据则必须调用此函数来读取出当前的错误码，
			// 千万不能省略这一步（即使你可能不想知道错误码是什么）
			VCI_ReadErrInfo(m_devtype, m_devind, m_CANInd, &errinfo);
			/* 异常报错弹框 */
			// RecvFrame_ErrorFormation(&errinfo);
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
#if 1 // 通过获取到的dbc文件对CAN解析
				CAN_Show_Info dbc_queue;
				dbc_queue.CanInfo = frameinfo[i];
				pthread->m_dbc_recv_queue.push(dbc_queue);
#else
				// 如果是数据帧，获取数据帧里面的数据
				if (frameinfo[i].RemoteFlag == 0 && frameinfo[i].ExternFlag == 0)
				{
					if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758)
					{
						printf("0x%03X\n", frameinfo[i].ID);
						CAN_Show_Info caninfo;
						caninfo.CanInfo = frameinfo[i];
						pthread->m_ssrInfo_queue.push(caninfo);
					}
					else if (frameinfo[i].ID == 0x760)
					{
						////  10次解析一次
						if (clac_760_times > 20)
						{
							clac_760_times = 0;
							pthread->vehicleCan_info.right_turn = frameinfo[i].Data[0] & 0x01;
							pthread->vehicleCan_info.left_turn = (frameinfo[i].Data[0] >> 1) & 0x01;
							pthread->vehicleCan_info.reversing = (frameinfo[i].Data[0] >> 3) & 0x03;
							pthread->vehicleCan_info.brake_status = (frameinfo[i].Data[0] >> 5) & 0x03;
							pthread->vehicleCan_info.car_speed = ((float)(frameinfo[i].Data[1] | (frameinfo[i].Data[2] << 8)) - 0.5f) / 3.6f / 128.0f;

							if (frameinfo[i].Data[3] == 0xFF && frameinfo[i].Data[4] == 0xFF)
								pthread->vehicleCan_info.yaw_rate = 0.0f;
							else
								pthread->vehicleCan_info.yaw_rate = (float)(((frameinfo[i].Data[3] + frameinfo[i].Data[4] * 255) - 0.5f) / 100.0 - 327.67);

							if (frameinfo[i].Data[5] == 0xFF && frameinfo[i].Data[6] == 0xFF)
								pthread->vehicleCan_info.x_accel = 0.0f;
							else
								pthread->vehicleCan_info.x_accel = (float)(((frameinfo[i].Data[5] + frameinfo[i].Data[6] * 255) - 0.5f) * 16.0 / 32768.0 - 16.0);
						}
						else
							clac_760_times++;
					}
				}
#endif
			}
		}
	}
	printf("recv thread end\n");
	return 0;
}
// bool CAutelAS100Dlg::start_CAN(int devInd, enum _BaudRate BaudRat)
bool start_CAN()
{
	VCI_INIT_CONFIG init_config;

	init_config.AccCode = 0;		  // 验收码
	init_config.AccMask = 0xFFFFFFFF; // 屏蔽码
	init_config.Filter = 0;			  // 滤波方式
	init_config.Mode = 0;			  // 工作模式
	init_config.Timing0 = 0x00;		  // 定时器0
	init_config.Timing1 = 0x1C;		  // 定时器1
	///* 设置波特率 */
	// switch (BaudRat){
	//	case BR_10K:		// 10Kbps
	//	{
	//		init_config.Timing0 = 0x31;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_20K:		// 20Kbps
	//	{
	//		init_config.Timing0 = 0x18;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_50K:		// 50Kbps
	//	{
	//		init_config.Timing0 = 0x09;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_125K:		// 125Kbps
	//	{
	//		init_config.Timing0 = 0x03;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_250K:		// 250Kbps
	//	{
	//		init_config.Timing0 = 0x01;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_500K:		// 500Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_800K:		// 800Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x16;	// 定时器1
	//	}break;
	//	case BR_1000K:		// 1000Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x14;	// 定时器1
	//	}break;
	//	default:break;
	// }

	/* 打开USBCAN设备 */
	if (VCI_OpenDevice(m_devtype, m_devind, 5) != STATUS_OK) // 500Kbps
	{
		printf("打开设备失败!\n");
		return false;
	}
	else
	{
		printf("打开USBCAN设备成功\n");
	}

	/* 初始化USBCAN设备 */
	if (VCI_InitCAN(m_devtype, m_devind, m_CANInd, &init_config) != STATUS_OK)
	{
		printf("初始化CAN失败!\n");
		VCI_CloseDevice(m_devtype, m_devind);
		return false;
	}
	else
	{
		printf("初始化USBCAN设备成功!\n");
	}

	if (VCI_StartCAN(m_devtype, m_devind, m_CANInd) == 1)
	{
		printf("启动成功\n");
	}
	else
	{
		printf("启动失败\n");
		return false;
	}

	return true;
}

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum {IDD = IDD_ABOUTBOX};
#endif

protected:
	virtual void DoDataExchange(CDataExchange *pDX); // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CAutelAS100Dlg 对话框

CAutelAS100Dlg::CAutelAS100Dlg(CWnd *pParent /*=nullptr*/)
	: CDialogEx(IDD_AUTEL_AS100___DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_devtype = VCI_USBCAN2;
	m_devind = 0;
	m_CANInd = 0;
	memset(&vehicleCan_info, 0, sizeof(struct VehicleCan));
	for (int i = 0; i < 3; i++)
	{
		memset(&m_srr_obstacle_info[i], 0, sizeof(struct SRRObstacle));
	}
	// txtFile_handle = NULL;
	m_thread_recv_is_stop = false;
	m_use_dbc_info_parse_thread_is_stop = false;
	m_thread_process_is_stop = false;
	m_thread_csv_is_stop = false;

	m_btn_func_sw = false;
	m_parse_process_stt = false;
	m_listbox_is_show = false;
	m_enable_time_filter = false;
	m_seeked_start_time = false;

	frm_cnt = 0;
	dbc_parse_ok = false;
	// csvFile_handle = NULL;
	m_csv_is_reading = false;
}

inline void myFree(void *p)
{
	if (p != NULL)
	{
		free(p);
		p = NULL;
	}
}
CAutelAS100Dlg::~CAutelAS100Dlg()
{
	m_thread_process_is_stop = true;

	// if (txtFile_handle != NULL) {
	//	fclose(txtFile_handle);
	//	txtFile_handle = NULL;
	// }

	// if (csvFile_handle != NULL) {
	// csvFile_handle->Close();
	//	myFree(csvFile_handle);
	//}

	if (m_thread_recv_is_stop == false)
	{
		m_thread_recv_is_stop = true;
	}

	if (m_thread_csv_is_stop == false)
	{
		m_thread_csv_is_stop = true;
	}
}

void CAutelAS100Dlg::DoDataExchange(CDataExchange *pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit_object_num_handle);
	DDX_Control(pDX, IDC_EDIT2, m_edit_move_object_num_handle);
	DDX_Control(pDX, IDC_EDIT3, m_edit_stop_object_num_handle);
	DDX_Control(pDX, IDC_EDIT4, m_edit_static_object_num_handle);
	DDX_Control(pDX, IDC_LIST1, m_lis_show_info_handle);
	DDX_Control(pDX, IDC_EDIT5, m_edit_vehicle_spd_handle);
	DDX_Control(pDX, IDC_EDIT6, m_imu_x_acc_handle);
	DDX_Control(pDX, IDC_EDIT7, m_imu_z_yaw_handle);
	DDX_Control(pDX, IDC_COMBO2, m_combox_can_index_handle);
	DDX_Control(pDX, IDC_BUTTON1, m_btn_func_sw_handle);
	DDX_Control(pDX, IDC_MFCEDITBROWSE1, m_editBrowse_handle);
	DDX_Control(pDX, IDC_BUTTON2, m_btn_cvs_analysis_handle);
	DDX_Control(pDX, IDC_COMBO1, m_combox_show_listbox_handle);
	DDX_Control(pDX, IDC_COMBO3, m_combox_begin_hour_handle);
	DDX_Control(pDX, IDC_COMBO4, m_combox_begin_min_handle);
	DDX_Control(pDX, IDC_COMBO5, m_combox_end_hour_handle);
	DDX_Control(pDX, IDC_COMBO6, m_combox_end_min_handle);
	DDX_Control(pDX, IDC_COMBO7, m_combox_begin_second_handle);
	DDX_Control(pDX, IDC_COMBO8, m_combox_end_second_handle);
	DDX_Control(pDX, IDC_MFCEDITBROWSE_dbc, m_edtBrowse_dbc_handle);
	DDX_Control(pDX, IDC_COMBO9, m_cmbox_csv_offset_handle);
}

BEGIN_MESSAGE_MAP(CAutelAS100Dlg, CDialogEx)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDC_BUTTON1, &CAutelAS100Dlg::OnBnClickedButton1)
ON_WM_TIMER()
ON_BN_CLICKED(IDC_BUTTON2, &CAutelAS100Dlg::OnBnClickedButton2)
END_MESSAGE_MAP()

bool CAutelAS100Dlg::mySetFontSize(int ID, float font_size)
{
	LOGFONT logfont; // 最好弄成类成员,全局变量,静态成员
	CFont *pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = (LONG)(logfont.lfHeight * font_size); // 这里可以修改字体的高比例
	logfont.lfWidth = (LONG)(logfont.lfWidth * font_size);	 // 这里可以修改字体的宽比例
	static CFont font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}

void CAutelAS100Dlg::Dlg_Front_Init()
{
#define CONTROLER_SIZE 1.5f
	mySetFontSize(IDC_LIST1, CONTROLER_SIZE - 0.2f); // 展示区

	mySetFontSize(IDC_BUTTON1, CONTROLER_SIZE); // 打开

	mySetFontSize(IDC_STATIC1, CONTROLER_SIZE); // 所有障碍物
	mySetFontSize(IDC_EDIT1, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC2, CONTROLER_SIZE); // 移动障碍物
	mySetFontSize(IDC_EDIT2, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC3, CONTROLER_SIZE); // 停止障碍物
	mySetFontSize(IDC_EDIT3, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC4, CONTROLER_SIZE); // 静止障碍物
	mySetFontSize(IDC_EDIT4, CONTROLER_SIZE);

	mySetFontSize(IDC_STATIC5, CONTROLER_SIZE); // 车速
	mySetFontSize(IDC_EDIT5, CONTROLER_SIZE);	// 车速

	mySetFontSize(IDC_STATIC6, CONTROLER_SIZE); // 加速度
	mySetFontSize(IDC_EDIT6, CONTROLER_SIZE);	//

	mySetFontSize(IDC_STATIC7, CONTROLER_SIZE); // 拐弯角度
	mySetFontSize(IDC_EDIT7, CONTROLER_SIZE);	//

	// csv文件解析
	mySetFontSize(IDC_BUTTON2, CONTROLER_SIZE);		   //
	mySetFontSize(IDC_MFCEDITBROWSE1, CONTROLER_SIZE); //

	mySetFontSize(IDC_COMBO2, CONTROLER_SIZE);	// CAN标识ID
	mySetFontSize(IDC_STATIC8, CONTROLER_SIZE); // CAN标识ID

	// 加载dbc文件
	mySetFontSize(IDC_MFCEDITBROWSE_dbc, CONTROLER_SIZE); // CAN标识ID
	mySetFontSize(IDC_COMBO9, CONTROLER_SIZE);			  // CAN标识ID

	// CAN下标添加标识
	for (int i = 0; i < 8; i++)
	{
		char index_str[4] = {0};
		sprintf(index_str, "%d", i);
		m_combox_can_index_handle.InsertString(i, index_str);
	}
	m_combox_can_index_handle.SetCurSel(0);

	mySetFontSize(IDC_COMBO1, CONTROLER_SIZE); // 是否显示内容listbox
	m_combox_show_listbox_handle.InsertString(0, "不显示");
	m_combox_show_listbox_handle.InsertString(1, "显示");
	m_combox_show_listbox_handle.SetCurSel(0);
	// 时间小时分钟 combox控件】
	mySetFontSize(IDC_COMBO3, CONTROLER_SIZE); // 时间标识ID
	mySetFontSize(IDC_COMBO4, CONTROLER_SIZE); // 标识ID
	mySetFontSize(IDC_COMBO5, CONTROLER_SIZE); // 标识ID
	mySetFontSize(IDC_COMBO6, CONTROLER_SIZE); // 标识ID
	mySetFontSize(IDC_COMBO7, CONTROLER_SIZE); // 标识ID
	mySetFontSize(IDC_COMBO8, CONTROLER_SIZE); // 标识ID

	mySetFontSize(IDC_DBC_FILE_STA, CONTROLER_SIZE);   // DBC文件提示
	mySetFontSize(IDC_EXCEL_FILE_STA, CONTROLER_SIZE); // excel文件提示
	mySetFontSize(IDC_HMS_TIME_STA, CONTROLER_SIZE);   // 时分秒提示
	mySetFontSize(IDC_STA_ZHI, CONTROLER_SIZE);		   // 至

	for (int i = 0; i <= 24; i++)
	{
		char index_str[4] = {0};
		sprintf(index_str, "%d", i);
		m_combox_begin_hour_handle.InsertString(i, index_str);
		m_combox_end_hour_handle.InsertString(i, index_str);
	}
	m_combox_begin_hour_handle.SetCurSel(12);
	m_combox_end_hour_handle.SetCurSel(12);
	for (int i = 0; i <= 60; i++)
	{
		char index_str[4] = {0};
		sprintf(index_str, "%d", i);
		m_combox_begin_min_handle.InsertString(i, index_str);
		m_combox_end_min_handle.InsertString(i, index_str);
	}
	m_combox_begin_min_handle.SetCurSel(30);
	m_combox_end_min_handle.SetCurSel(30);

	for (int i = 0; i <= 60; i++)
	{
		char index_str[4] = {0};
		sprintf(index_str, "%d", i);
		m_combox_begin_second_handle.InsertString(i, index_str);
		m_combox_end_second_handle.InsertString(i, index_str);
	}
	m_combox_begin_second_handle.SetCurSel(30);
	m_combox_end_second_handle.SetCurSel(30);

	// csv生成数据的偏移量（为了让数据与表头对其）
	for (int i = 0; i <= 5; i++)
	{
		char index_str[4] = {0};
		sprintf(index_str, "%d", i);
		m_cmbox_csv_offset_handle.InsertString(i, index_str);
	}
	m_cmbox_csv_offset_handle.SetCurSel(0);
}
// CAutelAS100Dlg 消息处理程序
bool test_parameter(void *ret)
{
	// int* p = (int*)ret;
	//*p = 123;

	*(int *)ret = 123;
	return true;
}
bool test_parameter_char(void *ret)
{
	char tmp[6] = "12363";
	strcpy((char *)ret, tmp);
	//*(char*)(ret[0]) = tmp[0];
	// ret[1] = '\0';
	return true;
}
BOOL CAutelAS100Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu *pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);	 // 设置大图标
	SetIcon(m_hIcon, FALSE); // 设置小图标

	// TODO: 在此添加额外的初始化代码
	AllocConsole();						 // 开辟控制台
	SetConsoleTitle(_T("Debug Dialog")); // 设置控制台窗口标题
	freopen("CONOUT$", "w", stdout);	 // 重定向输出
	printf("*******************Debug*******************");

	SetBackgroundColor(RGB(120, 163, 222)); // 浅色蓝色1

	// 设置文字大小
	Dlg_Front_Init();

	// 固定大小
	::SetWindowLong(m_hWnd, GWL_STYLE, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);

	// 浏览文件
	m_editBrowse_handle.EnableFileBrowseButton(_T(""), _T("加载csv文件 (*.csv)|*.csv|All Files (*.*)|*.*|"));

	// 加载dbc文件
	m_edtBrowse_dbc_handle.EnableFileBrowseButton(_T(""), _T("加载dbc文件 (*.dbc)|*.dbc|All Files (*.*)|*.*|"));

	// 启动数据处理线程
	m_thread_process_is_stop = false;
	AfxBeginThread(Can_Process_Thread, this);

	return TRUE; // 除非将焦点设置到控件，否则返回 TRUE
}

void CAutelAS100Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CAutelAS100Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 当用户拖动最小化窗口时系统调用此函数取得光标
// 显示。
HCURSOR CAutelAS100Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// 开始采集按钮--执行逻辑
void CAutelAS100Dlg::OnBnClickedButton1()
{
	// 若解析中就要提示要先关闭，在执行
	if (m_parse_process_stt)
	{
		List_Show_Info("请先结束解析，再执行采集。");
		return;
	}
	// printf("00000000000\n");
	if (m_btn_func_sw)
	{
		m_btn_func_sw = false;

		if (task_end_opt() == false)
			return;

		VCI_CloseDevice(m_devtype, m_devind);

		// KillTimer(1);		// 关闭车速等信息的显示
		m_btn_func_sw_handle.SetWindowTextA("开始采集");
	}
	else
	{

		if (load_can_dbc_cfg_file_opt() == false)
			return;

		// 获取CAN索引
		m_devind = m_combox_can_index_handle.GetCurSel();
		if (start_CAN() == false)
		{
			List_Show_Info("CAN打开失败.");
			return;
		}

		m_btn_func_sw = true;
		m_btn_func_sw_handle.SetWindowTextA("结束采集");
		/* 开启接受线程 */
		m_thread_recv_is_stop = false;
		AfxBeginThread(Can_Recv_Thread, this);
		m_thread_process_is_working = true;

		// SetTimer(1, 100, NULL);			// 显示车速
	}
}
/*
 * 移位操作
 * 返回位移对应的量
 */
uint8_t Shift_Opertion(int shift_num)
{
	uint8_t ret_value = 0;
	switch (shift_num)
	{
	case 1:
		ret_value = 0x01;
		break;
	case 2:
		ret_value = 0x03;
		break;
	case 3:
		ret_value = 0x07;
		break;
	case 4:
		ret_value = 0x0F;
		break;
	case 5:
		ret_value = 0x1F;
		break;
	case 6:
		ret_value = 0x3F;
		break;
	case 7:
		ret_value = 0x7F;
		break;
	case 8:
		ret_value = 0xFF;
		break;
	default:
		ret_value = 0;
	}

	// printf(">>0x%02X\n", ret_value);
	return ret_value;
}

uint8_t Motorola_Shift_Value(int shift_num)
{
	uint8_t ret_value = 0;
	switch (shift_num)
	{
	case 1:
		ret_value = 0x80;
		break;
	case 2:
		ret_value = 0xC0;
		break;
	case 3:
		ret_value = 0xE0;
		break;
	case 4:
		ret_value = 0xF0;
		break;
	case 5:
		ret_value = 0xF8;
		break;
	case 6:
		ret_value = 0xFC;
		break;
	case 7:
		ret_value = 0xFE;
		break;
	case 8:
		ret_value = 0xFF;
		break;
	default:
		ret_value = 0;
	}

	// printf(">>0x%02X\n", ret_value);
	return ret_value;
}

UINT CAutelAS100Dlg::use_can_dbc_cfg_to_unpack_csv_file_opt_thread(void *param)
{
#define DIRECT_PRINT 0
#define OUTPUT_CSV_FILE 1

	CAutelAS100Dlg *pthread = reinterpret_cast<CAutelAS100Dlg *>(param);
	printf("处理dbc逻辑\n");
	int clac_760_times = 0;
	char time_str[64] = {0}; // 时间戳写csv文件使用
	CStringA w_csv_str("");	 // 将要写在csv文件的数据
	bool is_ValueTable = false;
	uint8_t is_finish_times = 0; // 用于判断是否解析完毕csv文件，进而关闭本线程

	while (pthread->m_use_dbc_info_parse_thread_is_stop == false)
	{
		// if (!pthread->m_thread_process_is_working) Sleep(100);

		if (pthread->m_dbc_recv_queue.empty() == false)
		{
			is_finish_times = 0;

			CAN_Show_Info l_can_data = pthread->m_dbc_recv_queue.front();
			// 步骤1：遍历所有的frameid有多少条
			for (int frm_i = 0; frm_i < pthread->frm_cnt; frm_i++)
			{
				// 更新list显示信息状态
				if (pthread->m_combox_show_listbox_handle.GetCurSel() == 0)
					pthread->m_listbox_is_show = false;
				else
					pthread->m_listbox_is_show = true;

				// 步骤2：找frameid与已知（在dbc文件配置项）的相同的项
				if (l_can_data.CanInfo.frame_id == pthread->m_can_dbc_handle[frm_i].frame_id)
				{
					// char show_buf[1024] = { 0 }; // 在listbox显示中使用
					char *show_buf = new char[512];
					// Ret_Signal_Str ret_signal;
					// printf("-------------------------\n>>%04X:\t", pthread->m_can_dbc_handle[frm_i].msg.frame_id);
					// for (int ii = 0; ii < 8; ii++) {
					//	printf("%02X ", l_can_data.CanInfo.Data[ii]);
					// }
					// printf("\n");
					//  步骤3：判断时间戳，能从excel取出来就用，否则就加载实时系统时间
					if (strlen(l_can_data.TimeStr) > 5)
					{
						memcpy(time_str, l_can_data.TimeStr, sizeof(l_can_data.TimeStr));

						if (pthread->m_listbox_is_show)
						{
							sprintf(show_buf, "%s  ", l_can_data.TimeStr);
						}
					}
					else
					{
						SYSTEMTIME st;
						GetLocalTime(&st);
						sprintf(time_str, "%02d-%02d_%02d:%02d:%02d.%03d  ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

						if (pthread->m_listbox_is_show)
						{
							sprintf(show_buf, "%02d-%02d_%02d:%02d:%02d.%03d  ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
						}
					}

					// 步骤4：从当前的frameid中找出来所有的signal信息
					for (int sig_i = 0; sig_i < pthread->m_can_dbc_handle[frm_i].sig_cnt; sig_i++)
					{
						// 步骤5：解析出数据（通过解析dbc属性的库函数）
						float unpack_float_val = use_dbc_cfg_params_unpack_can_msg_api(&pthread->m_can_dbc_handle[frm_i].sig[sig_i].dbc_cfg, (uint8_t *)l_can_data.CanInfo.Data, (uint32_t)0);
						// printf("dbc res:%10.2f\r\n", unpack_float_val);
						//
						//  步骤6：根据配置的数据类型，对signal进行分类处理，并显示在界面中或写到csv文件中
						switch (pthread->m_can_dbc_handle[frm_i].sig[sig_i].dbc_cfg.data_type)
						{
						case DATA_TYPE_IS_INT: // 整型
						{
							if (pthread->m_can_dbc_handle[frm_i].sig[sig_i].enm_cnt > 0) // 有vector table 项
							{
								for (int enm_i = 0; enm_i < pthread->m_can_dbc_handle[frm_i].sig[sig_i].enm_cnt; enm_i++)
								{
									if ((int)unpack_float_val == pthread->m_can_dbc_handle[frm_i].sig[sig_i].enum_value[enm_i].val)
									{
										// 赋值字符串
										if (pthread->m_listbox_is_show) // listbox显示
										{
											sprintf(show_buf + strlen(show_buf), "%s:%s", pthread->m_can_dbc_handle[frm_i].sig[sig_i].name,
													pthread->m_can_dbc_handle[frm_i].sig[sig_i].enum_value[enm_i].describe);
										}
										w_csv_str.AppendFormat(",%s", pthread->m_can_dbc_handle[frm_i].sig[sig_i].enum_value[enm_i].describe);
										is_ValueTable = true;
										break;
									}
								}
								// 若在Value table中找到了对应值，就直接结束
								if (is_ValueTable)
								{
									is_ValueTable = false;
									break;
								}

								if (pthread->m_listbox_is_show) // listbox显示
								{
									sprintf(show_buf + strlen(show_buf), "%s:%d", pthread->m_can_dbc_handle[frm_i].sig[sig_i].name, (int)unpack_float_val);
								}
								w_csv_str.AppendFormat(",%d", (int)unpack_float_val);
							}
							else // 无vector table 项
							{
								if (pthread->m_listbox_is_show) // listbox显示
								{
									sprintf(show_buf + strlen(show_buf), "%s:%d", pthread->m_can_dbc_handle[frm_i].sig[sig_i].name, (int)unpack_float_val);
								}
								w_csv_str.AppendFormat(",%d", (int)unpack_float_val);
							}
						}
						break;
						case DATA_TYPE_IS_FLT: // 浮点类型-float
						{
							if (pthread->m_listbox_is_show) // listbox显示
							{
								sprintf(show_buf + strlen(show_buf), "%s:%0.4f", pthread->m_can_dbc_handle[frm_i].sig[sig_i].name, unpack_float_val);
							}
							w_csv_str.AppendFormat(",%0.4f", unpack_float_val);
							// printf("float:%10.2f\r\n", unpack_float_val);
						}
						break;
						}

						// 增加字段之间的分隔符
						if (pthread->m_listbox_is_show) // listbox显示
						{
							if (sig_i != (pthread->m_can_dbc_handle[frm_i].sig_cnt - 1))
							{
								sprintf(show_buf + strlen(show_buf), ",  ");
							}
						}
					}
					// 将数据写入到csv文件中
					pthread->write_data_to_csv_file_opt(w_csv_str, frm_i, time_str, l_can_data.CanInfo.frame_id);
					w_csv_str.Empty(); // clear

					printf("isempty():%d,buf_len:%d\r\n", w_csv_str.IsEmpty(), strlen(show_buf));

					// 在listbox中显示
					if (pthread->m_listbox_is_show)
					{
						sprintf(show_buf + strlen(show_buf), "\n");
						pthread->List_Show_Info(show_buf);
					}
					// free
					if (show_buf != NULL)
						delete[] show_buf;
				}
			}
			pthread->m_dbc_recv_queue.pop();
		}
		else
		{
			// 读取csv操作结束了，解析动作也可以结束了。若有需要，可以再次触发（按下开始解析按钮即可）
			if (pthread->m_thread_csv_is_stop == true)
			{
				pthread->m_use_dbc_info_parse_thread_is_stop = true;
			}
			Sleep(2);
		}
	}

	// 在显示list中，添加生成文件的路径
	pthread->m_w_csv_file_name; // 文件名字
	// 获取当前文件路径
	TCHAR l_current_path[MAX_PATH] = {0};

	// 获取当前文件路径
	GetCurrentDirectory(MAX_PATH, l_current_path);
	CString l_show_w_csv_file_path("");
	l_show_w_csv_file_path = l_show_w_csv_file_path + l_current_path + "\\" + pthread->m_w_csv_file_name;
	pthread->List_Show_Info(l_show_w_csv_file_path.GetBuffer());

	// 显示结束读取csv文件
	pthread->List_Show_Info("解析csv文件结束");
	pthread->List_Show_Info("");
	printf(">>>>>>处理dbc数据结束\n");

	if (w_csv_str.IsEmpty() == false)
		w_csv_str.Empty();				 // clear data
	pthread->m_csv_wfile_handle.Close(); // 关闭write csv file handle\

	pthread->m_btn_cvs_analysis_handle.SetWindowTextA("开始解析");
	pthread->task_end_opt();
	printf("用dbc配置文件解析csv文件操作-结束.\r\n");
	return 0;
}

UINT CAutelAS100Dlg::Can_Process_Thread(void *param)
{
#define DEBUG_PRINT 0
#define JUDGE_PKG_NUM 9
	/** 得到本类的指针 */
	CAutelAS100Dlg *pthread = reinterpret_cast<CAutelAS100Dlg *>(param);
	struct Judge_Str l_judge_str = {false, 1, 0, 0, 0, 0, 0, 0, 0};
	// 用于一次性显示数据
	queue<struct SRRObstacle> l_show_queue;
	// queue<struct Show_Info_Str> l_show_queue;
	// struct Show_Info_Str l_shwo_str;
	printf("处理逻辑\n");

	while (!pthread->m_thread_process_is_stop)
	{

		if (!pthread->m_thread_process_is_working)
			Sleep(100);
		// Sleep(1);
		//  异常处理，若长度超出9帧就开始处理
		// if (pthread->can_handle->m_ssrInfo_queue.size() > 9)
		//	goto START_ANALYSIS;

		if (!l_judge_str.is_full_9_frame_sw) // 超过9帧就处理
		{
			if (!pthread->m_ssrInfo_queue.empty())
			{
				if (pthread->m_ssrInfo_queue.size() < JUDGE_PKG_NUM)
					continue;
				else
				{
					// START_ANALYSIS:
					l_judge_str.pop_cnt = 1;
					l_judge_str.array_index_multi = 0;
					l_judge_str.move_obj_num = 0;
					l_judge_str.static_obj_num = 0;
					l_judge_str.stop_obj_num = 0;
					l_judge_str.is_full_9_frame_sw = true;
					l_judge_str.object_num = pthread->m_ssrInfo_queue.size();
					// 仅处理9的倍数条数据
					l_judge_str.dispose_times = l_judge_str.object_num / JUDGE_PKG_NUM * JUDGE_PKG_NUM;
					printf("queue size:%d,dipose_times:%d\n", l_judge_str.object_num, l_judge_str.dispose_times);
				}
			}
		}
		else
		{
			// 一次性处理9的倍数包的数据
			if (l_judge_str.pop_cnt > l_judge_str.dispose_times)
			{
				l_judge_str.is_full_9_frame_sw = false;
				// printf("object_dispose_num:%d\n", l_judge_str.pop_cnt-1);
				//  贴数据
				while (!l_show_queue.empty())
				{
#if 1
					struct SRRObstacle show_info = l_show_queue.front();
#else
					struct Show_Info_Str show_info = l_show_queue.front();
#endif
					l_show_queue.pop();
					pthread->In_Listbox_Show_SRR_Info(&show_info);
				}
				continue;
			}
			else
			{
				l_judge_str.pop_cnt++;
			}

			CAN_Show_Info m_can_info = pthread->m_ssrInfo_queue.front();
			pthread->m_ssrInfo_queue.pop();

			// 分去除不完整的包解析数据，和不去除完整包进行解析显示
			switch (m_can_info.CanInfo.frame_id)
			{
			case 0x756:
			case 0x753:
			case 0x750:
			{
				if (m_can_info.CanInfo.frame_id == 0x756)
					l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.frame_id == 0x753)
					l_judge_str.array_index = 1;
				else
					l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 = m_can_info.CanInfo.Data[0];		   // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].object_id = m_can_info.CanInfo.Data[1];			   // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].warning_grade = m_can_info.CanInfo.Data[2];		   // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].tracking_num = m_can_info.CanInfo.Data[3];		   // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].ttc.x = (float)(m_can_info.CanInfo.Data[4] * 0.05f); // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].ttc.y = (float)(m_can_info.CanInfo.Data[5] * 0.05f); // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].warning_zone = (m_can_info.CanInfo.Data[6] & 0x0F);  // 4bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].motion_state = (m_can_info.CanInfo.Data[6] >> 4);	   // 4bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].malfunction = (m_can_info.CanInfo.Data[7] & 0x0F);   // 4bit

				// if(pthread->m_srr_obstacle_info[l_judge_str.array_index].object_id == 0)
				//	printf("dispose_track_num:%d\n", m_can_info.CanInfo.Data[3]);
				//  计算障碍物的个数
				switch (pthread->m_srr_obstacle_info[l_judge_str.array_index].motion_state)
				{
				case UNKNOWN_MOTION_STATE:
				{
				}
				break;
				case MOVING_MOTION_STATE:
				{
					pthread->m_srr_obstacle_info[l_judge_str.array_index].move_obj_num++;
				}
				break;
				case STOPED_MOTION_STATE:
				{
					pthread->m_srr_obstacle_info[l_judge_str.array_index].stop_obj_num++;
				}
				break;
				case STANDING_MOTION_STATE:
				{
					pthread->m_srr_obstacle_info[l_judge_str.array_index].static_obj_num++;
				}
				break;
				}

				pthread->m_srr_obstacle_info[l_judge_str.array_index].system_time = 0; // m_can_info.CanInfo.recv_time_ms;

				// l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]); // 测试
			}
			break;
			case 0x757:
			case 0x754:
			case 0x751:
			{
				if (m_can_info.CanInfo.frame_id == 0x757)
					l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.frame_id == 0x754)
					l_judge_str.array_index = 1;
				else
					l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 = m_can_info.CanInfo.Data[0];																   // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].dist.x = (float)(m_can_info.CanInfo.Data[2] << 8 | m_can_info.CanInfo.Data[1]) * 0.02f - 655.36f;			   // 16bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].dist.y = (float)((m_can_info.CanInfo.Data[4] & 0x0F) << 4 | m_can_info.CanInfo.Data[3]) * 0.02f - 40.96f;	   // 12bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].vel.x = (float)(m_can_info.CanInfo.Data[5] << 4 | (m_can_info.CanInfo.Data[4] & 0xF0) >> 4) * 0.1f - 204.8f; // 12bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].vel.y = (float)((m_can_info.CanInfo.Data[7] & 0x03) << 2 | m_can_info.CanInfo.Data[6]) * 0.1f - 51.2f;	   // 10bit

				//if (pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 && \
				//	pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3) {
				// l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]);
				// }
			}
			break;
			case 0x758:
			case 0x755:
			case 0x752:
			{

				if (m_can_info.CanInfo.frame_id == 0x758)
					l_judge_str.array_index = 2;
				else if (m_can_info.CanInfo.frame_id == 0x755)
					l_judge_str.array_index = 1;
				else
					l_judge_str.array_index = 0;

				pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3 = m_can_info.CanInfo.Data[0];
				pthread->m_srr_obstacle_info[l_judge_str.array_index].acc.x = (float)(m_can_info.CanInfo.Data[4]) * 0.2f - 25.6f; // 8bit
				pthread->m_srr_obstacle_info[l_judge_str.array_index].acc.y = (float)(m_can_info.CanInfo.Data[5]) * 0.2f - 25.6f; // 8bit

				if (strlen(m_can_info.TimeStr) > 2)
				{
					pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time = true;
					memcpy(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, m_can_info.TimeStr, strlen(m_can_info.TimeStr)); // 时间戳
				}
				else
				{
					pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time = false;
					memset(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, 0, sizeof(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str));
				}

				if (pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order1 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 &&
					pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order2 == pthread->m_srr_obstacle_info[l_judge_str.array_index].frame_order3)
				{
#if 1 // 将0x760包中的车速信息一起加到queue包中
					if (pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_time)
					{
						while (!pthread->m_canSpeed_queue.empty())
						{
							CAN_Show_Info l_carSpeed_info = pthread->m_canSpeed_queue.front();

							// 时间判断条件，到百毫秒级别 16:59:27:4
							if (strncmp(pthread->m_srr_obstacle_info[l_judge_str.array_index].time_stamp_str, l_carSpeed_info.TimeStr, 10) == 0)
							{
								// 找到了就解析，车速、加速度、角速度信息，并添加到
								float car_speed = 0.0f;
								float car_acceleration = 0.0f;
								float car_angle_speed = 0.0f;
								// pthread->vehicleCan_info.right_turn = frameinfo[i].Data[0] & 0x01;
								// pthread->vehicleCan_info.left_turn = (frameinfo[i].Data[0] >> 1) & 0x01;
								// pthread->vehicleCan_info.reversing = (frameinfo[i].Data[0] >> 3) & 0x03;
								// pthread->vehicleCan_info.brake_status = (frameinfo[i].Data[0] >> 5) & 0x03;

								car_speed = ((float)(l_carSpeed_info.CanInfo.Data[1] | (l_carSpeed_info.CanInfo.Data[2] << 8)) - 0.5f) / 3.6f / 128.0f;

								if (l_carSpeed_info.CanInfo.Data[3] == 0xFF && l_carSpeed_info.CanInfo.Data[4] == 0xFF)
									car_angle_speed = 0.0f;
								else
									car_angle_speed = (float)(((l_carSpeed_info.CanInfo.Data[3] + l_carSpeed_info.CanInfo.Data[4] * 255) - 0.5f) / 100.0 - 327.67);

								if (l_carSpeed_info.CanInfo.Data[5] == 0xFF && l_carSpeed_info.CanInfo.Data[6] == 0xFF)
									car_acceleration = 0.0f;
								else
									car_acceleration = (float)(((l_carSpeed_info.CanInfo.Data[5] + l_carSpeed_info.CanInfo.Data[6] * 255) - 0.5f) * 16.0 / 32768.0 - 16.0);

								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_speed = car_speed;
								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_acceleration = car_acceleration;
								pthread->m_srr_obstacle_info[l_judge_str.array_index].car_angle_speed = car_angle_speed;

								// 是否启动一个触发开关
								pthread->m_srr_obstacle_info[l_judge_str.array_index].is_valid_carSpeed = true;
								break;
							}

							pthread->m_canSpeed_queue.pop();
						}
					}
#endif
					l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]);
				}
				// l_show_queue.push(pthread->m_srr_obstacle_info[l_judge_str.array_index]); // 测试
				memset(&pthread->m_srr_obstacle_info[l_judge_str.array_index], 0, sizeof(struct SRRObstacle));
			}
			break;
			}
		}
	}
	printf("处理数据结束\n");
	return 0;
}

void CAutelAS100Dlg::In_Listbox_Show_SRR_Info(struct SRRObstacle *p_info)
{
	if (p_info->tracking_num <= 0)
		return; // 跟踪障碍物个数为0
	if (p_info->object_id == 0xFF)
		return; // 无效障碍物

	char print_buf[250] = {0};

	if (p_info->is_valid_time && strlen(p_info->time_stamp_str) > 5)
	{
		p_info->is_valid_time = false;
		sprintf(print_buf, "%s  ", p_info->time_stamp_str);
	}
	else
	{
		SYSTEMTIME st;
		GetLocalTime(&st);
		// printf("%4d-%02d-%02d %02d:%02d:%02d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
		sprintf(print_buf, "%02d-%02d %02d:%02d:%02d.%03d  ", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	}
	if (p_info->object_id < 10)
		sprintf(print_buf + strlen(print_buf), "ID:   %d ", p_info->object_id);
	else if (p_info->object_id < 100)
		sprintf(print_buf + strlen(print_buf), "ID:  %d ", p_info->object_id);
	else
		sprintf(print_buf + strlen(print_buf), "ID:%d ", p_info->object_id);

	switch (p_info->warning_grade)
	{
	case 0:
		sprintf(print_buf + strlen(print_buf), "预警:无  ");
		break;
	case 1:
		sprintf(print_buf + strlen(print_buf), "预警:绿  ");
		break;
	case 2:
		sprintf(print_buf + strlen(print_buf), "预警:黄  ");
		break;
	case 3:
		sprintf(print_buf + strlen(print_buf), "预警:声  ");
		break;
	default:
		sprintf(print_buf + strlen(print_buf), "预警:无  ");
		break;
	}

	if (p_info->tracking_num < 10)
		sprintf(print_buf + strlen(print_buf), "标记ID:   %d  ", p_info->tracking_num);
	else if (p_info->tracking_num < 100)
		sprintf(print_buf + strlen(print_buf), "标记ID: %d  ", p_info->tracking_num);
	else
		sprintf(print_buf + strlen(print_buf), "标记ID:%d  ", p_info->tracking_num);

	switch (p_info->warning_zone)
	{
	case UNKNOWN_CAR_ZONE:
		sprintf(print_buf + strlen(print_buf), "待定");
		break;
	case RIGHT_MIDDLE_CAR_ZONE:
		sprintf(print_buf + strlen(print_buf), "右侧");
		break;
	case RIGHT_BACK_CAR_ZONE:
		sprintf(print_buf + strlen(print_buf), "右后");
		break;
	case RIGHT_FRONT_CAR_ZONE:
		sprintf(print_buf + strlen(print_buf), "右前");
		break;
	default:
		sprintf(print_buf + strlen(print_buf), "待定");
		break;
	}

	switch (p_info->motion_state)
	{
	case UNKNOWN_MOTION_STATE:
		sprintf(print_buf + strlen(print_buf), "|待定 ");
		break;
	case MOVING_MOTION_STATE:
		sprintf(print_buf + strlen(print_buf), "|移动 "); /*p_info->move_obj_num++;*/
		break;
	case STOPED_MOTION_STATE:
		sprintf(print_buf + strlen(print_buf), "|停止 "); /*p_info->stop_obj_num++;*/
		break;
	case STANDING_MOTION_STATE:
		sprintf(print_buf + strlen(print_buf), "|静止 "); /* p_info->static_obj_num++; */
		break;
	default:
		sprintf(print_buf + strlen(print_buf), "|待定 ");
		break;
	}

	sprintf(print_buf + strlen(print_buf), "TTC(X:%6.2f, ", p_info->ttc.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->ttc.y);

	sprintf(print_buf + strlen(print_buf), "距离:(X:%6.2f, ", p_info->dist.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->dist.y);

	sprintf(print_buf + strlen(print_buf), "速度(X:%6.2f, ", p_info->vel.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f)  ", p_info->vel.y);

	sprintf(print_buf + strlen(print_buf), "加速度(X:%6.2f, ", p_info->acc.x);
	sprintf(print_buf + strlen(print_buf), "Y:%6.2f) ", p_info->acc.y);

	// 添加车速度、加速度、角速度信息
	if (p_info->is_valid_carSpeed)
	{
		p_info->is_valid_carSpeed = false;
		sprintf(print_buf + strlen(print_buf), "车速:%6.2f, ", p_info->car_speed);
		sprintf(print_buf + strlen(print_buf), "加速度:%6.2f, ", p_info->car_acceleration);
		sprintf(print_buf + strlen(print_buf), "角速度:%6.2f\n", p_info->car_angle_speed);
	}
	else
	{
		sprintf(print_buf + strlen(print_buf), "\n");
	}

	// switch (p_info->malfunction)
	//  {
	// case SENSOR_OK:sprintf(print_buf + strlen(print_buf), "正常 "); break;
	// case SENSOR_FAILURE:sprintf(print_buf + strlen(print_buf), "传感器失败 "); break;
	// case SENSOR_PERFORM_DECREASED:sprintf(print_buf + strlen(print_buf), "执行力降低 "); break;
	// case SENSOR_COMMUNICATION_FAILURE:sprintf(print_buf + strlen(print_buf), "通讯失败 "); break;
	// default:sprintf(print_buf + strlen(print_buf), "正常 "); break;
	// }

	// sprintf(print_buf + strlen(print_buf), "用时:%lld\n", p_info->system_time);
	if (m_listbox_is_show)
	{
		List_Show_Info(print_buf);
	}

	// if (txtFile_handle != NULL)
	//  {
	//	// 写文件
	//	fwrite(print_buf, sizeof(char), strlen(print_buf), txtFile_handle);
	//	// 存储10000条，就换文件存储
	//	if (write_times > 30000) {
	//		write_times = 0;
	//		fclose(txtFile_handle);
	//		Create_txt_File_To_Save_Log_Info();
	//	}
	//	else write_times++;
	// }

	// 显示检测到障碍物的个数
	CString cstr_num("");
	p_info->total_obj_num = p_info->move_obj_num + p_info->stop_obj_num + p_info->static_obj_num;
	cstr_num.Format("%d", p_info->total_obj_num);
	// cstr_num.Format("%d", p_info->tracking_num);
	m_edit_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->move_obj_num);
	m_edit_move_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->stop_obj_num);
	m_edit_stop_object_num_handle.SetWindowTextA(cstr_num);

	cstr_num.Format("%d", p_info->static_obj_num);
	m_edit_static_object_num_handle.SetWindowTextA(cstr_num);
}

void CAutelAS100Dlg::List_Show_Info(char *data)
{
	printf("%s\r\n", data);
	CString str(data);
	m_lis_show_info_handle.AddString(str);

	// 自动滚动
	m_lis_show_info_handle.SetCurSel(m_lis_show_info_handle.GetCount() - 1);
}

// void CAutelAS100Dlg::Create_txt_File_To_Save_Log_Info()
//{
//	// 打开文件,写数据
//	// 获取当前文件路径
//	char m_hexFile_path[MAX_PATH];			// hex配置文件路径
//	TCHAR sHexPath[MAX_PATH] = { 0 };
//	GetCurrentDirectory(MAX_PATH, sHexPath);
//	strcpy(m_hexFile_path, sHexPath);
//
//	SYSTEMTIME st;
//	GetLocalTime(&st);
//
//	char l_fileName[100] = { 0 };
//	sprintf(l_fileName, "\\CAN_Parse_Result_%04d%02d%02d%02d%02d%02d.txt", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
//	strcat(m_hexFile_path, l_fileName);
//	//printf("path:%s\n", m_hexFile_path);
//
//	CString m_file_name("");
//	m_file_name = m_hexFile_path;
//	errno_t err = _wfopen_s(&txtFile_handle, m_file_name.AllocSysString(), L"w"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
//	if (err == NULL) //判断文件是否打开成功
//	{
//		//List_Show_Info("打开文件失败。");
//		return ;
//	}
// }

bool CAutelAS100Dlg::create_csv_file_to_save_unpack_res_opt()
{
	bool res = false;

	// 在当前路径下生成一个csv文件
	SYSTEMTIME sysLocaltime;
	GetLocalTime(&sysLocaltime);
	m_w_csv_file_name.Format(_T("can_parse_result_%04i%02i%02i%02i%02i%02i.csv"), sysLocaltime.wYear, sysLocaltime.wMonth,
							 sysLocaltime.wDay, sysLocaltime.wHour, sysLocaltime.wMinute, sysLocaltime.wSecond);

	// 创建文件
	if (m_csv_wfile_handle.Open(m_w_csv_file_name, CFile::modeWrite | CFile::modeCreate) == false)
	{
		List_Show_Info("创建存储数据cvs文件失败!");
		return res;
	}

	CStringA csv_strbuf(""); // 显示内容
	// 若加载dbc文件成功，加入表头内容（字段）
	if (dbc_parse_ok)
	{
		// printf("----------\n");
		csv_strbuf.Format("日期,帧ID"); // date,frame id
		// msg
		for (uint8_t msg_i = 0; msg_i < frm_cnt; msg_i++)
		{
			// signal
			for (uint8_t sig_i = 0; sig_i < m_can_dbc_handle[msg_i].sig_cnt; sig_i++)
			{
				if (strlen(m_can_dbc_handle[msg_i].sig[sig_i].unit) > 0)
				{
					csv_strbuf.AppendFormat(",%s [%s]", m_can_dbc_handle[msg_i].sig[sig_i].name, m_can_dbc_handle[msg_i].sig[sig_i].unit);
				}
				else
				{
					csv_strbuf.AppendFormat(",%s", m_can_dbc_handle[msg_i].sig[sig_i].name);
				}
			}
		}
		csv_strbuf.AppendFormat("\r\n");
		res = true;
	}
	else
	{
		printf("dbc文件未解析，未添加cvs表头内容\n");
	}

	m_csv_wfile_handle.Write(csv_strbuf.GetString(), csv_strbuf.GetLength()); // 写入内容
	csv_strbuf.Empty();

	return res;
}

bool CAutelAS100Dlg::write_data_to_csv_file_opt(CStringA w_cstr, uint8_t msg_index, char *time_str, uint32_t frame_id)
{
	CStringA csv_strbuf("");
	printf("时间戳:%s\r\n", time_str);
	// 时间
	csv_strbuf.Format("%s,0x%08X", time_str, frame_id);
	int column_seq = 0; // 列号
	bool add_coloumn_sw = false;

	// msg
	for (uint8_t msg_i = 0; msg_i < frm_cnt; msg_i++)
	{
		if (msg_i == msg_index) // 要写数据
		{
			int csv_col_offset = m_cmbox_csv_offset_handle.GetCurSel();
			for (int i = 0; i < csv_col_offset; i++)
			{
				csv_strbuf.AppendFormat(",");
			}
			csv_strbuf += w_cstr;
		}
		else // 其他信号数据直接清空
		{
			for (uint8_t sig_i = 0; sig_i < m_can_dbc_handle[msg_i].sig_cnt; sig_i++)
			{
				csv_strbuf.AppendFormat(","); // 赋值空
				column_seq++;
			}
		}
	}
	csv_strbuf += ",\r\n";

	m_csv_wfile_handle.Write(csv_strbuf.GetString(), csv_strbuf.GetLength()); // 写入内容

	return true;
}
void CAutelAS100Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialogEx::OnTimer(nIDEvent);

	switch (nIDEvent)
	{
	case 0:
	{ // 50ms清空一次listbox数据
	  // int count = m_lis_show_info_handle.GetCount();
	  // for (int i = count; i >= 0; i--) {
	  //	m_lis_show_info_handle.DeleteString(i);
	  // }

		// 清空数据显示缓冲区
		// memset(m_srr_obstacle_info, 0, sizeof(struct SRRObstacle) * OBSTACLES_GROUP_NUM);
	}
	break;
	case 1:
	{ // 显示车速等信息
		CString show_info_str("");
		show_info_str.Format("%0.2f", vehicleCan_info.car_speed);
		m_edit_vehicle_spd_handle.SetWindowTextA(show_info_str);

		show_info_str.Format("%0.2f", vehicleCan_info.x_accel);
		m_imu_x_acc_handle.SetWindowTextA(show_info_str);

		show_info_str.Format("%0.2f", vehicleCan_info.yaw_rate);
		m_imu_z_yaw_handle.SetWindowTextA(show_info_str);
	}
	break;
	}
}

#define READ_CSV_LEN 20
typedef struct _Read_Mark_Str
{
	char timestamp[READ_CSV_LEN];
	bool ssrInfo_isValid;
	bool carSpeed_isValid;
} Read_Mark_Str;
Read_Mark_Str read_mark_str = {0, false, false};

#define LIMITED_SSR_SPEED 0 // 针对角雷达和车速定制版
UINT CAutelAS100Dlg::unpack_csv_src_file_thread(void *param)
{
	CAutelAS100Dlg *pthread = (CAutelAS100Dlg *)param;

	// CAN_Show_Info frameinfo_ssr_csv = { 0 };
	// CAN_Show_Info frameinfo_carSpeed_csv = { 0 };
	CAN_Show_Info unpack_csv_info = {0};
	unsigned char cTemp;
	char buf[READ_CSV_LEN] = {0}; // 定义缓冲区
	int i = 0;
	memset(buf, 0, READ_CSV_LEN); // 缓冲区清零
	int nIndex = 0;
	int j = 0;

	printf("unpack csv file thread\n");
	pthread->List_Show_Info("开始解析csv文件...");

	long l_csv_rfile_length = (long)pthread->m_csv_rfile_handle.GetLength(); // 以字节获取文件当前的逻辑长度
	pthread->m_csv_is_reading = true;

	// 有条件过滤是否要中止动作
	bool has_time_filter_is_terminate = false;

	// 主要内容
	while ((pthread->m_thread_csv_is_stop == false) && (i < l_csv_rfile_length))
	{
		i++;
		pthread->m_csv_rfile_handle.Read(&cTemp, 1); // 从文件中读取一个字节
		if (cTemp == '\n')							 // 是否读取到了换行符
		{
			nIndex = 0;
			memset(&unpack_csv_info, 0, sizeof(CAN_Show_Info));
			// memset(&frameinfo_carSpeed_csv, 0, sizeof(CAN_Show_Info));
		}
		if ((cTemp == ',') || (cTemp == '\n')) // 是否读取到了逗号或者换行符,即得到了一个完整数据
		{
			switch (nIndex)
			{
			case 2: // 在时间上添加过滤条件
			{
				if (pthread->m_enable_time_filter == true) // 时间过滤条件，开启
				{
					if (pthread->m_seeked_start_time == false) // 时间没有触发前，找开始时间
					{
						memset(pthread->m_time_begin_filter_str, 0, READ_CSV_LEN);
						if (strncmp(pthread->m_time_begin_filter_str, buf, 8) == 0) // 找到开始时间，就认为事件触发了
						{
							pthread->m_seeked_start_time = true;
							// printf("找到开始时间：%s\r\n", pthread->m_time_begin_filter_str);
						}
						else
						{
							j = 0;
							memset(buf, 0, sizeof(buf));
							memset(&read_mark_str, 0, sizeof(Read_Mark_Str));
							continue;
						}
					}
					else // 事件触发了，找结束（停止）时间
					{
						memset(pthread->m_time_end_filter_str, 0, READ_CSV_LEN);
						if (strncmp(pthread->m_time_end_filter_str, buf, 8) == 0) // 找到结束时间，就认为事件结束了
						{
							pthread->m_seeked_start_time = false;
							pthread->m_enable_time_filter = false;

							has_time_filter_is_terminate = true;
							// printf("找到结束时间：%s\r\n", pthread->m_time_end_filter_str);
						}
					}
					memcpy(read_mark_str.timestamp, buf, j);
				}
				else
				{
					memcpy(read_mark_str.timestamp, buf, j);
				}
			}
			break;
			case 3: // frameid
			{
#if LIMITED_SSR_SPEED // 针对角雷达和车速定制版
				if (pthread->m_seeked_start_time)
				{ // 触发了才会执行
					if (strncmp(buf, "0x750", 5) == 0 ||
						strncmp(buf, "0x751", 5) == 0 ||
						strncmp(buf, "0x752", 5) == 0 ||
						strncmp(buf, "0x753", 5) == 0 ||
						strncmp(buf, "0x754", 5) == 0 ||
						strncmp(buf, "0x755", 5) == 0 ||
						strncmp(buf, "0x756", 5) == 0 ||
						strncmp(buf, "0x757", 5) == 0 ||
						strncmp(buf, "0x758", 5) == 0)
					{
						// printf("\n%s  ", read_mark_str.timestamp);
						memcpy(unpack_csv_info.TimeStr, read_mark_str.timestamp, strlen(read_mark_str.timestamp));
						read_mark_str.ssrInfo_isValid = true;
						// printf("%s  ", buf);
						unpack_csv_info.CanInfo.ID = (buf[2] - 0x30) * 16 * 16 + (buf[3] - 0x30) * 16 + (buf[4] - 0x30);
						// printf("0x%03X  ", unpack_csv_info.CanInfo.ID);
					}
					else
					{
						read_mark_str.ssrInfo_isValid = false;
					}
				}

				// 找到0x760数据入队，为了找到相同时间的车速、加速度、转角信息
				if (strncmp(buf, "0x760", 5) == 0)
				{
					memcpy(frameinfo_carSpeed_csv.TimeStr, read_mark_str.timestamp, strlen(read_mark_str.timestamp));
					read_mark_str.carSpeed_isValid = true;
					// printf("%s  ", buf);
					frameinfo_carSpeed_csv.CanInfo.ID = (buf[2] - 0x30) * 16 * 16 + (buf[3] - 0x30) * 16 + (buf[4] - 0x30);
					// printf("0x%03X  ", frameinfo_csv.ID);
				}
				else
				{
					read_mark_str.carSpeed_isValid = false;
				}
#else
				memcpy(unpack_csv_info.TimeStr, read_mark_str.timestamp, strlen(read_mark_str.timestamp));
				// printf("%s  ", buf);

				char *end;
				unpack_csv_info.CanInfo.frame_id = strtol(buf, &end, 16);
				if (end == buf) // 没有进行有效转换
				{
					// printf("No digits were found\n");
					unpack_csv_info.CanInfo.frame_id = 0;
				}
				else
				{
				}
				// printf("0x%08X\r\n", unpack_csv_info.CanInfo.ID);
#endif
			}
			break;
			case 4:	 // data[0]
			case 5:	 // data[1]
			case 6:	 // data[2]
			case 7:	 // data[3]
			case 8:	 // data[4]
			case 9:	 // data[5]
			case 10: // data[6]
			case 11: // data[7]
			{
#if LIMITED_SSR_SPEED // 针对角雷达和车速定制版
				if (read_mark_str.ssrInfo_isValid)
				{ // 角雷达信息
					UINT8 data = 0;
					if (j == 1)
					{
						if (buf[0] >= 'a' && buf[0] <= 'f')
							data = buf[0] - 0x57;
						else
							data = buf[0] - 0x30;
					}
					else
					{
						// 0 high; 1 low
						if (buf[0] >= 'a' && buf[0] <= 'f')
							data = (buf[0] - 0x57) * 16;
						else
							data = (buf[0] - 0x30) * 16;

						if (buf[1] >= 'a' && buf[1] <= 'f')
							data += buf[1] - 0x57;
						else
							data += buf[1] - 0x30;
					}

					unpack_csv_info.CanInfo.Data[nIndex - 4] = data;
					// printf("%02X  ", data);
					//  封包
					if (nIndex == 11)
					{
#if 0
							pthread->m_ssrInfo_queue.push(unpack_csv_info);
#else
						pthread->m_dbc_recv_queue.push(unpack_csv_info);
#endif
					}
				}

				if (read_mark_str.carSpeed_isValid)
				{ // 车速信息
					UINT8 data = 0;
					if (j == 1)
					{
						if (buf[0] >= 'a' && buf[0] <= 'f')
							data = buf[0] - 0x57;
						else
							data = buf[0] - 0x30;
					}
					else
					{
						// 0 high; 1 low
						if (buf[0] >= 'a' && buf[0] <= 'f')
							data = (buf[0] - 0x57) * 16;
						else
							data = (buf[0] - 0x30) * 16;

						if (buf[1] >= 'a' && buf[1] <= 'f')
							data += buf[1] - 0x57;
						else
							data += buf[1] - 0x30;
					}

					frameinfo_carSpeed_csv.CanInfo.Data[nIndex - 4] = data;

					// 封包
					if (nIndex == 11)
					{
						pthread->m_canSpeed_queue.push(frameinfo_carSpeed_csv);
					}
				}
#else
				UINT8 data = 0;
				if (j == 1)
				{
					if (buf[0] >= 'a' && buf[0] <= 'f')
						data = buf[0] - 0x57;
					else
						data = buf[0] - 0x30;
				}
				else
				{
					// 0 high; 1 low
					if (buf[0] >= 'a' && buf[0] <= 'f')
						data = (buf[0] - 0x57) * 16;
					else
						data = (buf[0] - 0x30) * 16;

					if (buf[1] >= 'a' && buf[1] <= 'f')
						data += buf[1] - 0x57;
					else
						data += buf[1] - 0x30;
				}

				unpack_csv_info.CanInfo.Data[nIndex - 4] = data;
				// printf("%02X  ", data);
				//  封包
				if (nIndex == 11)
				{
					// printf("+++++++++++++++\n");
					unpack_csv_info.CanInfo.DataLen = 8;
					//pthread->m_dbc_recv_queue.push(unpack_csv_info);
					if (has_time_filter_is_terminate == true)
					{
						has_time_filter_is_terminate = false;
						// 避免在用dbc配置解析csv文件时，解析不全就提前中止的问题。
						//Sleep(20);
						pthread->m_thread_csv_is_stop = true;
					}
				}
#endif
			}
			break;
			default:
				break;
			}
			nIndex++;

			memset(buf, 0, READ_CSV_LEN); // 清空缓冲区

			j = 0;
			continue;
		}
		buf[j++] = cTemp;
	}

	pthread->m_csv_is_reading = false;
	pthread->m_csv_rfile_handle.Close();
	memset(buf, 0, sizeof(buf));
	printf("读csv文件操作结束\r\n");
	return 0;
}
// 开始解析按钮-执行逻辑
void CAutelAS100Dlg::OnBnClickedButton2()
{
	if (m_btn_func_sw)
	{
		List_Show_Info("请先停止采集，再执行解析。");
		return;
	}

	if (m_parse_process_stt == true) // 正解析中，强制关闭
	{
		printf("手动强制停止解析csv文件。\r\n");
		m_use_dbc_info_parse_thread_is_stop = true;
		return;
		// if (task_end_opt() == false) return;
		// m_btn_cvs_analysis_handle.SetWindowTextA("开始解析");
	}
	else // 打开
	{
		// 获取读取文件地址和名称
		CString cvs_name("");
		m_editBrowse_handle.GetWindowTextA(cvs_name);
		if (cvs_name.GetLength() <= 2)
		{
			List_Show_Info("请先加载csv文件。");
			return;
		}

		// 打开将要解析的cvs配置文件
		if (m_csv_rfile_handle.Open(cvs_name, CFile::modeRead) == FALSE) // CFile::modeRead打开并读取文件
		{
			List_Show_Info("导入的清单文件打不开! 可能已被别的编辑器打开了，请关闭后再试!");
			m_csv_rfile_handle.Close();
			return;
		}

		if (load_can_dbc_cfg_file_opt() == false)
			return;

		// return;
		//  获取过滤数据的起止时间
		if (judge_csv_file_time_condition() == false)
		{
			List_Show_Info("不满足时间过滤的条件，请检查过滤时间设置...");
			return;
		}

		// 创建线程，线程处理数据
		m_thread_csv_is_stop = false;
		AfxBeginThread(unpack_csv_src_file_thread, this);
		// m_thread_process_is_working = true;

		m_parse_process_stt = true;
		m_btn_cvs_analysis_handle.SetWindowTextA("结束解析");
	}
}

// -----------------------------------------------------------------------
// Convert signal encoding to a string
// -----------------------------------------------------------------------
const char *show_byte_order_opt(uint8_t byte_order)
{
	if (byte_order == INTEL_MODE)
	{
		return "Intel";
	}
	else if (byte_order == MOTOROLA_MODE)
	{
		return "Motorola";
	}
	else
	{
		return "Undefined";
	}
}

// -----------------------------------------------------------------------
// Convert signal representation to a string
// -----------------------------------------------------------------------
const char *show_data_type_opt(uint8_t data_type)
{
	if (data_type == DATA_TYPE_VALID)
		return "invalid data type";
	else if (data_type == DATA_TYPE_IS_INT)
		return "signed / unsigned";
	else if (data_type == DATA_TYPE_IS_FLT)
		return "float";
	// else if (data_type == DATA_TYPE_IS_DBL)  return "double";
	else
		return "undefined";
}

// -----------------------------------------------------------------------
//
// Dump the contents of a database to stdout
//
// -----------------------------------------------------------------------
KvaDbStatus CAutelAS100Dlg::unpack_can_dbc_file_opt(char *filename)
{
	KvaDbStatus status = kvaDbOK;
	KvaDbHnd dh = 0;
	KvaDbMessageHnd mh = 0;
	KvaDbSignalHnd sh = 0;
	unsigned int flags = 0;

	// Open a database handle
	status = kvaDbOpen(&dh);
	if (status != kvaDbOK)
	{
		printf("Could not create a database handle: %d\n", status);
		return status;
	}

	// Load the database file
	status = kvaDbReadFile(dh, filename);
	if (status != kvaDbOK)
	{
		printf("Could not load '%s': %d\n", filename, status);
		return status;
	}

	status = kvaDbGetFlags(dh, &flags);
	if (status != kvaDbOK)
	{
		printf("kvaDbGetFlags failed: %d\n", status);
		return status;
	}
	// printf("  Database: flags=0x%x\n", flags);

	// Get the first message in the database
	status = kvaDbGetFirstMsg(dh, &mh);
	if (status != kvaDbOK)
	{
		printf("kvaDbGetFirstMsg failed: %d\n", status);
		return status;
	}

	// Go through all messages in the database
	while (status == kvaDbOK) // 解析消息
	{
		if (frm_cnt >= DBC_MSG_NUM) // 帧ID的下标
		{
			printf("最多能解析%d条消息（帧ID数据）。\n", DBC_MSG_NUM);
			break;
		}

		char msg_name[DATA_LEN_32];
		// char msg_qname[DATA_LEN_64];
		// char msg_comment[DATA_LEN_100];
		int dlc = 0;
		unsigned int id = 0;

		// Clear the strings
		memset(msg_name, 0, sizeof(msg_name));
		// memset(msg_qname, 0, sizeof(msg_qname));
		// memset(msg_comment, 0, sizeof(msg_comment));

		// Get the properties for each message
		status = kvaDbGetMsgName(mh, msg_name, sizeof(msg_name)); // 帧名字
		if (status != kvaDbOK)
		{
			printf("kvaDbGetMsgName failed: %d\n", status);
			return status;
		}

		// status = kvaDbGetMsgQualifiedName(mh, msg_qname, sizeof(msg_qname));	// 帧关系属性
		// if (status != kvaDbOK) {
		//	printf("kvaDbGetMsgQualifiedName failed: %d\n", status);
		//	return status;
		// }

		// status = kvaDbGetMsgComment(mh, msg_comment, sizeof(msg_comment));	// 帧描述
		// if (status != kvaDbOK) {
		//	printf("kvaDbGetMsgComment failed: %d\n", status);
		//	return status;
		// }

		status = kvaDbGetMsgId(mh, &id, &flags); // 帧ID--frame_id
		if (status != kvaDbOK)
		{
			printf("kvaDbGetMsgId failed: %d\n", status);
			return status;
		}

		status = kvaDbGetMsgDlc(mh, &dlc); // data 长度
		if (status != kvaDbOK)
		{
			printf("kvaDbGetMsgDlc failed: %d\n", status);
			return status;
		}

		//----- 步骤1：获取帧name,frameid,dlc ------
		// 消息：动态分配大小

		memcpy(m_can_dbc_handle[frm_cnt].name, msg_name, strlen(msg_name));
		// memcpy(m_can_dbc_handle[frm_cnt].qname, msg_qname, strlen(msg_qname));
		// memcpy(m_can_dbc_handle[frm_cnt].commit, msg_comment, strlen(msg_comment));
		uint8_t is_extern_frame = (id >> 31) & 0x01; // 是否是扩展帧
		if (is_extern_frame == 0x01)				 // 扩展帧
		{
			m_can_dbc_handle[frm_cnt].frame_id = (UINT32)id & 0x1FFFFFFF; // 29 bit
		}
		else
		{
			m_can_dbc_handle[frm_cnt].frame_id = (UINT32)id & 0x7FF; // 11 bit
		}
		// m_can_dbc_handle[frm_cnt].msg.dlc = (UINT8)dlc;
		// m_can_dbc_handle[frm_cnt].msg.flags = (UINT8)flags;		// 没有用到

		// printf("[Debug]msg_cnt:%d\t", frm_cnt);

		//----- 步骤2：获取 信号name,信号unit,dlc ------
		int sig_cnt = 0;
		// Go through all signals for this message
		status = kvaDbGetFirstSignal(mh, &sh);
		while (status == kvaDbOK) // 解析信号
		{
			if (sig_cnt >= DBC_SIG_NUM) // 帧ID的下标
			{
				printf("每条消息中最多能解析%d条信号。\n", DBC_SIG_NUM);
				break;
			}

			char signal_name[DATA_LEN_32];
			// char signal_qname[DATA_LEN_64];
			// char signal_comment[DATA_LEN_100];
			char signal_unit[DATA_LEN_16];
			KvaDbSignalEncoding byte_order;
			KvaDbSignalType sig_data_type;
			// 缺少解析dbc文件中有无符号类型
			int start_bit = 0;
			int bit_count = 0;
			double minval = 0;
			double maxval = 0;
			double factor = 0;
			double offset = 0;

			// Reset the strings
			memset(signal_name, 0, sizeof(signal_name));
			// memset(signal_qname, 0, sizeof(signal_qname));
			// memset(signal_comment, 0, sizeof(signal_comment));
			memset(signal_unit, 0, sizeof(signal_unit));

			// Get the properties for each signal.
			status = kvaDbGetSignalName(sh, signal_name, sizeof(signal_name)); // 信号名字
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalName failed: %d\n", status);
				return status;
			}

			// status = kvaDbGetSignalQualifiedName(sh, signal_qname, sizeof(signal_qname));	// 类关系属性
			// if (status != kvaDbOK) {
			//	printf("kvaDbGetSignalQualifiedName failed: %d\n", status);
			//	return status;
			// }

			// status = kvaDbGetSignalComment(sh, signal_comment, sizeof(signal_comment));	// 描述
			// if (status != kvaDbOK) {
			//	printf("kvaDbGetSignalComment failed: %d\n", status);
			//	return status;
			// }

			status = kvaDbGetSignalUnit(sh, signal_unit, sizeof(signal_unit)); // 单位
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalUnit failed: %d\n", status);
				return status;
			}

			status = kvaDbGetSignalEncoding(sh, &byte_order); // 字节序
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalEncoding failed: %d\n", status);
				return status;
			}

			status = kvaDbGetSignalRepresentationType(sh, &sig_data_type); // 信号--数据类型
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalRepresentationType failed: %d\n", status);
				return status;
			}

			status = kvaDbGetSignalValueLimits(sh, &minval, &maxval); // 最小值，最大值
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalValueLimits failed: %d\n", status);
				return status;
			}

			status = kvaDbGetSignalValueScaling(sh, &factor, &offset); // 比例系数，补偿值
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalValueScaling failed: %d\n", status);
				return status;
			}

			status = kvaDbGetSignalValueSize(sh, &start_bit, &bit_count); // bit宽度
			if (status != kvaDbOK)
			{
				printf("kvaDbGetSignalValueSize failed: %d\n", status);
				return status;
			}

			// int sig_cnt = m_can_dbc_handle[frm_cnt].sig_cnt;

#if 1 // 获取帧信号可选枚举值表（信号值表）
			KvaDbEnumValueHnd eh;
			status = kvaDbGetFirstEnumValue(sh, &eh);
			// if (status != kvaDbOK) {
			//	// 若信号没有信号值表就执行到这里
			//	//printf("kvaDbGetFirstEnumValue failed: %d\n", status);
			// }
			// else {
			//	printf("\n%s\n", signal_name);
			// }

			while (status == kvaDbOK) // 解析枚举值消息 VAL_开头的数据
			{
				int val = 0;
				char buf[DATA_LEN_32] = {0};
				int enm_cnt = 0;

				status = kvaDbGetFirstEnumValuePair(sh, &eh, &val, buf, sizeof(buf)); // 获取val_tab所有值
				while (status == kvaDbOK)
				{
					if (enm_cnt >= DBC_ENUM_VAL_NUM) // 信号值列表的下标
					{
						printf("每条信号中最多能解析%d个信号值列表。\n", DBC_ENUM_VAL_NUM);
						break;
					}
					// printf("buf:%s,\t\tval:%d\n", buf, val);
					//  赋值
					memcpy(m_can_dbc_handle[frm_cnt].sig[sig_cnt].enum_value[enm_cnt].describe, buf, strlen(buf)); // val_tab 描述
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].enum_value[enm_cnt].val = val;						   // val_tab 值

					status = kvaDbGetNextEnumValuePair(sh, &eh, &val, buf, sizeof(buf));
					enm_cnt++;
				}
				status = kvaDbGetNextEnumValue(sh, &eh);
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].enm_cnt = enm_cnt; // val_tab 数量
			}
			// printf("enm_cnt:%d\n", m_can_dbc_handle[frm_cnt].sig[sig_cnt].enm_cnt);
#endif
			// 信号：动态分配大小
			memcpy(m_can_dbc_handle[frm_cnt].sig[sig_cnt].name, signal_name, strlen(signal_name));
			memcpy(m_can_dbc_handle[frm_cnt].sig[sig_cnt].unit, signal_unit, strlen(signal_unit));
			// memcpy(m_can_dbc_handle[frm_cnt].sig[sig_cnt].qname, signal_qname, strlen(signal_qname));
			// memcpy(m_can_dbc_handle[frm_cnt].sig[sig_cnt].commit, signal_comment, strlen(signal_comment));
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.min = (float)minval;
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.max = (float)maxval;
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.factor = (float)factor;
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.offset = (float)offset;
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.start_bit = start_bit;
			m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.bit_count = bit_count;
			switch (byte_order) // 字节序
			{
			case kvaDb_Intel:
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.byte_order = INTEL_MODE;
				break;
			case kvaDb_Motorola:
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.byte_order = MOTOROLA_MODE;
				break;
			default:
				break;
			}

			switch (sig_data_type) // signal数据类型
			{
			case kvaDb_Invalid:
			{
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_VALID;
			}
			break;
			case kvaDb_Signed:
			{
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.sign_bit = SG_SIGNED; // 符号位

				if ((m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.factor == 1.0f) &&
					(m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.offset == 0.0f)) // 是小数就是float
				{
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_INT;
					printf("%s,signed,int\r\n", m_can_dbc_handle[frm_cnt].sig[sig_cnt].name);
				}
				else
				{
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_FLT;
					printf("%s,signed,float\r\n", m_can_dbc_handle[frm_cnt].sig[sig_cnt].name);
				}
			}
			break;
			case kvaDb_Unsigned:
			{

				if ((m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.factor == 1.0f) &&
					(m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.offset == 0.0f)) // 是小数就是float
				{
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.sign_bit = SG_UNSIGNED;
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_INT;
					printf("%s,unsigned,int\r\n", m_can_dbc_handle[frm_cnt].sig[sig_cnt].name);
				}
				else
				{
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_FLT;
					m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.sign_bit = SG_SIGNED;
					printf("%s,signed,float\r\n", m_can_dbc_handle[frm_cnt].sig[sig_cnt].name);
				}
			}
			break;
			case kvaDb_Float:
			{
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.sign_bit = SG_SIGNED;
				m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_FLT;
			}
			break;
			// case kvaDb_Double:	// 暂时不支持
			//{
			//	m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.sign_bit = SG_SIGNED;
			//	m_can_dbc_handle[frm_cnt].sig[sig_cnt].dbc_cfg.data_type = DATA_TYPE_IS_DBL;
			// }break;
			default:
				break;
			}

			status = kvaDbGetNextSignal(mh, &sh);
			sig_cnt++; // 信号下标志递增
					   // printf("[Debug]sig_cnt:%d\n", sig_cnt);
		}
		// sig_cnt = 0;
		m_can_dbc_handle[frm_cnt].sig_cnt = sig_cnt; // 信号下标
		status = kvaDbGetNextMsg(dh, &mh);
		frm_cnt++; // 消息下标递增
	}

	// 获取属性值（略）

	status = kvaDbClose(dh);
	if (status != kvaDbOK)
	{
		printf("kvaDbClose failed: %d\n", status);
		return status;
	}
	// printf("\n\n");
	return status;
}

bool CAutelAS100Dlg::load_can_dbc_cfg_file_opt(char *dbc_name)
{
	bool res = false;
	// clear
	frm_cnt = 0;
	for (int i = 0; i < DBC_MSG_NUM; i++)
	{
		memset(m_can_dbc_handle + i, 0, sizeof(CAN_DBC_RECV_INFO_ST));
	}

	int8_t res_stt = unpack_can_dbc_file_opt(dbc_name);
	switch (res_stt)
	{
	case kvaDbOK:
	{
		dbc_parse_ok = true;
		Debug_Print_DBC_Info();
		res = true;
	}
	break;
	case kvaDbErr_Fail:
		List_Show_Info("General failure");
		break;
	case kvaDbErr_NoDatabase:
		List_Show_Info(" No database was found");
		break;
	case kvaDbErr_Param:
		List_Show_Info("One or more of the parameters in call is erronous");
		break;
	case kvaDbErr_NoMsg:
		List_Show_Info("No message was found");
		break;
	case kvaDbErr_NoSignal:
		List_Show_Info("No signal was found");
		break;
	case kvaDbErr_Internal:
		List_Show_Info("An internal error occured in the library");
		break;
	case kvaDbErr_DbFileOpen:
		List_Show_Info("解析dbc文件失败，注意加载的 .dbc 文件名及路径中不应含有中文字符。");
		break;
	case kvaDbErr_DatabaseInternal:
		List_Show_Info("An internal error occured in the database handler");
		break;
	case kvaDbErr_NoNode:
		List_Show_Info("Could not find the database node");
		break;
	case kvaDbErr_NoAttrib:
		List_Show_Info("No attribute found");
		break;
	case kvaDbErr_OnlyOneAllowed:
		List_Show_Info("An identical kvaDbLib structure already exists (and only one database at a time can be used).");
		break;

	case kvaDbErr_WrongOwner:
		List_Show_Info("Wrong owner for attribute");
		break;
	case kvaDbErr_InUse:
		List_Show_Info("An item is in use");
		break;
	case kvaDbErr_BufferSize:
		List_Show_Info("The supplied buffer is too small to hold the result");
		break;
	case kvaDbErr_DbFileParse:
		List_Show_Info("Could not parse the database file");
		break;
	default:
		break;
	}

	return res;
}

void CAutelAS100Dlg::Debug_Print_DBC_Info()
{
	// printf("msg_cnt:%d\n\n", frm_cnt);
	for (int msg_i = 0; msg_i < frm_cnt; msg_i++)
	{
		printf("\n\n");
		printf("    Msg: name='%s'\n", m_can_dbc_handle[msg_i].name);
		// printf("         qname='%s', comment='%s'\n", m_can_dbc_handle[msg_i].msg.qname, m_can_dbc_handle[msg_i].msg.commit);
		// printf("         id=0x%X, dlc=%d, flags=%d\n", m_can_dbc_handle[msg_i].msg.frame_id, m_can_dbc_handle[msg_i].msg.dlc, m_can_dbc_handle[msg_i].msg.flags);
		// printf("         id=0x%X, dlc=%d\n", m_can_dbc_handle[msg_i].msg.frame_id, m_can_dbc_handle[msg_i].msg.dlc);
		printf("         id=0x%X\n", m_can_dbc_handle[msg_i].frame_id);
		// printf("sig_cnt:%d\n\n", m_can_dbc_handle[msg_i].sig_cnt );
		for (int sig_i = 0; sig_i < m_can_dbc_handle[msg_i].sig_cnt; sig_i++)
		{
			printf("      Signal: name='%s', unit='%s'\n", m_can_dbc_handle[msg_i].sig[sig_i].name, m_can_dbc_handle[msg_i].sig[sig_i].unit);
			// printf("              qname='%s'\n", m_can_dbc_handle[msg_i].sig[sig_i].qname);
			// printf("              comment='%s'\n", m_can_dbc_handle[msg_i].sig[sig_i].commit);
			printf("              byte_order='%s'\n", show_byte_order_opt(m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.byte_order));
			printf("              data_type='%s'\n", show_data_type_opt(m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.data_type));
			printf("              value min=%.02f, value max=%.02f\n", m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.min, m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.max);
			printf("              factor=%.02f, offset=%.02f\n", m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.factor, m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.offset);
			printf("              start_bit=%d, bit_count=%d\n", m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.start_bit, m_can_dbc_handle[msg_i].sig[sig_i].dbc_cfg.bit_count);
			// 打印信号值列表
			// printf("%d\n", m_can_dbc_handle[msg_i].sig[sig_i].enm_cnt);
			for (int enum_val_i = 0; enum_val_i < m_can_dbc_handle[msg_i].sig[sig_i].enm_cnt; enum_val_i++)
			{
				printf("                val:0x%02X, describle:%s\n", m_can_dbc_handle[msg_i].sig[sig_i].enum_value[enum_val_i].val,
					   m_can_dbc_handle[msg_i].sig[sig_i].enum_value[enum_val_i].describe);
			}
		}
	}
}

/*解析csv文件中的时间戳*/
bool CAutelAS100Dlg::judge_csv_file_time_condition()
{
	// 获取过滤数据的起止时间
	int begin_hour = m_combox_begin_hour_handle.GetCurSel();
	int begin_min = m_combox_begin_min_handle.GetCurSel();
	int begin_second = m_combox_begin_second_handle.GetCurSel();
	int end_hour = m_combox_end_hour_handle.GetCurSel();
	int end_min = m_combox_end_min_handle.GetCurSel();
	int end_second = m_combox_end_second_handle.GetCurSel();
	// printf("begin:%02d:%02d:%02d\n", begin_hour, begin_min, begin_second);
	// printf("end:%02d:%02d:%02d\n", end_hour, end_min, end_second);
	//  时间相同或开始就不过滤
	// if (begin_hour == 12 && begin_min == 30 && begin_second == 30 \
	//	&& end_hour == 12 && end_min == 30 && end_second == 30) 	// 不修改就不过滤
	//{
	//	m_enable_time_filter = false;
	//	m_seeked_start_time = true;
	// }
	// else
	m_enable_time_filter = false;
	m_seeked_start_time = true;
	memset(m_time_begin_filter_str, 0, sizeof(m_time_begin_filter_str));
	memset(m_time_end_filter_str, 0, sizeof(m_time_end_filter_str));

	if (begin_hour == end_hour && begin_min == end_min && begin_second == end_second) // 不修改就不过滤
	{
	}
	else if ((begin_hour > end_hour) ||
			 (begin_hour > end_hour && begin_min > end_min) ||
			 (begin_hour > end_hour && begin_min > end_min && begin_second > end_second))
	{
		List_Show_Info("不满足时间过滤的条件，请检查过滤时间设置...");
		m_csv_rfile_handle.Close();
		return false;
	}
	else
	{
		sprintf(m_time_begin_filter_str, "%02d:%02d:%02d", begin_hour, begin_min, begin_second);
		sprintf(m_time_end_filter_str, "%02d:%02d:%02d", end_hour, end_min, end_second);
		m_enable_time_filter = true;
		m_seeked_start_time = false;
		printf("start time:%s\r\n", m_time_begin_filter_str);
		printf("end time:%s\r\n", m_time_end_filter_str);
	}
	return true;
}

bool CAutelAS100Dlg::load_can_dbc_cfg_file_opt()
{
	bool res = false;
	// 先加载dbc文件
	CString dbc_name("");
	m_edtBrowse_dbc_handle.GetWindowTextA(dbc_name);
	if (dbc_name.IsEmpty() == true)
	{
		List_Show_Info("请先加载dbc文件。");
		return res;
	}

	// printf("%s\n", dbc_name.GetBuffer());
	if (load_can_dbc_cfg_file_opt(dbc_name.GetBuffer()) == false)
	{
		// 关闭对csv文件的访问
		m_csv_rfile_handle.Close();
		return res;
	}

	//// 创建txt存放解析结果
	// Create_txt_File_To_Save_Log_Info();

	// 创建csv文件存放解析结果
	create_csv_file_to_save_unpack_res_opt();

	// 获取Listbox是否显示内容
	if (m_combox_show_listbox_handle.GetCurSel() == 0)
	{
		m_listbox_is_show = false;
	}
	else
		m_listbox_is_show = true;

	// 加载CAN解析
	// AfxBeginThread(Can_Use_DBC_Process_Thread, this);
	AfxBeginThread(use_can_dbc_cfg_to_unpack_csv_file_opt_thread, this);
	m_use_dbc_info_parse_thread_is_stop = false;
	printf(">>>>>>>>已经打开\n");
	res = true;
	return res;
}

bool CAutelAS100Dlg::task_end_opt()
{
	m_parse_process_stt = false;
	m_thread_process_is_working = false;
	m_listbox_is_show = false;
	m_seeked_start_time = false;
	m_use_dbc_info_parse_thread_is_stop = true;

	// 若csv文件正在读取中，就强制停止
	if (m_csv_is_reading == true)
	{
		printf("强制终止操作\r\n");
		m_csv_is_reading = false;

		// clear queue
		while (m_dbc_recv_queue.empty() == false)
		{
			m_dbc_recv_queue.pop();
		}
		m_thread_csv_is_stop = true;
	}

	return true;
}
